# Open Distributed Factory (ODFactory)

## Description

Ce projet a pour objectif de fournir une architecture distribuée offrant des **services** de **machine learning**, comme par exemple le 'Natural Language Processing', sous la forme de micro-services ou de services classiques avec des modèles pré-entraînés.

Cette architecture se veut

* ouverte, donc extensible par ses utilisateurs qui peuvent ajouter d'autres modèles
* déployable sur site ou dans le cloud.

Afin de faciliter le déploiement des différents frameworks ainsi que la surveillance des serveurs, cette architecture s'appuie sur [Hortonworks HDP](https://fr.hortonworks.com/products/data-platforms/hdp/) et des **conteneurs Docker**.

## Démo. 1 : Analyse de sentiment avec des serveurs 'classiques'

Nous présentons ci-après un exemple d'application gérant les commentaires portés sur des films et permettant

* d'appliquer divers algorithmes de 'machine learning' afin de déterminer si un commentaire est positif ou négatif
* d'avoir un avis global du sentiment porté sur un film.

![alt text](architecture_globale.png "Architecture globale")

![alt text](ui-sentiment_analysis_2.png "Analyse de sentiments")

Cet exemple est basé sur les **frameworks**

* **Apache Cassandra** pour le stockage
* **Apache Kafka** pour la gestion des files de messages
* **Apache Spark** pour le traitement 'in memory' distribué et le 'machine learning'
* **Apache Hadoop** pour le système de fichiers distribué
* **NGINX**, **uWSGI** pour le serveur Internet
* **Docker** pour la création de conteneurs.

Les **langages de développement** sont

* **Scala** pour les serveurs ('backend')
* **Python/Flask** pour l'interface utilisateur ('frontend').

## Démo. 2 : Analyse de sentiment avec des micro-services

Ces exemples utilisent des micro-services d'ODFactory afin d'analyser des documents avec des algorithmes de 'Natural Language Processing'.

### Régression Logistique (SparkML)

#### Requête JSON Curl

```bash
curl --header "Content-Type: application/json" \
--request POST -i  --data '{"documents":{"items":[{"language":"en","text":"this is a good movie"},{"language":"en","text":"this is a bad movie"}]}}' \
http://localhost:8080/api/v1/sentiment/logisticregression
```

#### Réponse JSON

```bash
HTTP/1.1 200 OK
Content-Type: application/json
Content-Length: 384

{"sentiments":{"items":[{"language":"en","text":"this is a good movie","negativeProbability":0.4252780615039155,"positiveProbability":0.5747219384960846,"sentimentValue":1.0},{"language":"en","text":"this is a bad movie","negativeProbability":0.6279560482928339,"positiveProbability":0.3720439517071662,"sentimentValue":0.0}]},"api":{"name":"SentimentAnalysisService","version":"v1"}}
```

### Support Vector Machine (SparkML)

#### Requête JSON Curl

```bash
curl --header "Content-Type: application/json" \
--request POST -i  --data '{"documents":{"items":[{"language":"en","text":"this is a good movie"},{"language":"en","text":"this is a bad movie"}]}}' \
http://localhost:8080/api/v1/sentiment/vm
```

#### Réponse JSON

```bash
HTTP/1.1 200 OK
Content-Type: application/json
Content-Length: 324

{"sentiments":{"items":[{"language":"en","text":"this is a good movie","negativeProbability":0.0,"positiveProbability":0.0,"sentimentValue":1.0},{"language":"en","text":"this is a bad movie","negativeProbability":0.0,"positiveProbability":0.0,"sentimentValue":0.0}]},"api":{"name":"SentimentAnalysisService","version":"v1"}}
```

### Réseau de neurones Feed Forward (Keras/TensorFlow)

#### Requête JSON Curl

```bash
curl --header "Content-Type: application/json" \
--request POST -i  --data '{"documents":{"items":[{"language":"en","text":"this is a good movie"},{"language":"en","text":"this is a bad movie"}]}}' \
http://localhost:8080/api/v1/sentiment/feedforward
```

#### Réponse JSON

```bash
HTTP/1.0 200 OK
Content-Type: text/html; charset=utf-8
Content-Length: 340
Server: Werkzeug/0.15.4 Python/3.6.5
Date: Tue, 02 Jul 2019 01:23:00 GMT

{'api': {'name': 'FeedForwardService', 'version': 'v1'}, 'sentiments': {'items': [{'language': 'en', 'text': 'this is a good movie', 'negativeProbability': 0.0, 'positiveProbability': 0.0, 'sentimentValue': 1}, {'language': 'en', 'text': 'this is a bad movie', 'negativeProbability': 0.0, 'positiveProbability': 0.0, 'sentimentValue': 0}]}}
```

## Auteur

Fabrice LEBEL, fabrice.lebel.pro@outlook.com, [LinkedIn](https://www.linkedin.com/in/fabrice-lebel-b850674?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BbqGekl60S9W3uZDa6jyWkg%3D%3D) 

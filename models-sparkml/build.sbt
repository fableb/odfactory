import sbt.Keys.{dependencyOverrides, version}
import sbtassembly.AssemblyPlugin.autoImport.{ShadeRule, assemblyJarName, assemblyOption}

val sparkVersion = "2.1.0"

ThisBuild / scalaVersion := "2.11.8"

scalacOptions ++= Seq("-deprecation")

dependencyOverrides += "com.fasterxml.jackson.core" % "jackson-core" % "2.9.7"

dependencyOverrides += "com.fasterxml.jackson.core" % "jackson-databind" % "2.9.7"

dependencyOverrides += "com.fasterxml.jackson.module" % "jackson-module-scala_2.11" % "2.9.7"

resolvers += "Spark Packages Repo" at "https://dl.bintray.com/spark-packages/maven"

libraryDependencies ++= Seq(
  "com.typesafe" % "config" % "1.3.3",
  "io.circe" %% "circe-core" % "0.10.0",
  "io.circe" %% "circe-generic" % "0.10.0",
  "io.circe" %% "circe-parser" % "0.10.0",
  "org.apache.kafka" %% "kafka" % "2.1.0" withSources(),
  "org.apache.kafka" % "kafka-clients" % "2.1.0" withSources(),
  "com.datastax.spark" %% "spark-cassandra-connector" % "2.4.1",
  "commons-configuration" % "commons-configuration" % "1.9",
  "org.apache.spark" %% "spark-core" % sparkVersion % "provided" withSources(),
  "org.apache.spark" %% "spark-sql" % sparkVersion % "provided" withSources(),
  "org.apache.spark" %% "spark-mllib" % sparkVersion % "provided" withSources(),
  "org.scalatest" %% "scalatest" % "2.2.6" % "test"
)

excludeDependencies ++= Seq(
  ExclusionRule("com.github.jnr", "jnr-posix"),
  ExclusionRule("com.github.jnr", "jnr-ffi")//,
  //ExclusionRule("io.netty", "netty-all")
)

assemblyShadeRules in assembly := Seq(
  // Apache Spark crash avec le Guava de Google (v19)
  ShadeRule.rename("com.google.**" -> "repackaged.com.google.@1").inAll
)

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", "io.netty.versions.properties", xs@_*) => MergeStrategy.first
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    if (oldStrategy == MergeStrategy.deduplicate)
      MergeStrategy.first
    else
      oldStrategy(x)
}

unmanagedBase := baseDirectory.value / "lib"

lazy val `models-sparkml` = project
  .in(file("."))
  .settings(
    version := "0.1.0",
    name := "models-sparkml",
    assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false),
    assemblyJarName in assembly := s"${name.value}_${scalaBinaryVersion.value}-${sparkVersion}_${version.value}.jar"
  )
  .enablePlugins(AssemblyPlugin)
  //.aggregate(`comment-classifier`)

lazy val common = (project in file("odfactory/common"))
  .settings(
    version := "0.1.0",
    name := "common",
    assemblyJarName in assembly := s"${name.value}_${scalaBinaryVersion.value}-${sparkVersion}_${version.value}.jar"
  )

lazy val `logistic-regression` = (project in file("odfactory/models/nlp/sentiment/ODFLogisticRegression"))
  .settings(
    version := "0.1.0",
    name := "logistic-regression",
    mainClass in assembly := Some("ODFLogisticRegression"),
    assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false),
    assemblyJarName in assembly := s"${name.value}_${scalaBinaryVersion.value}-${sparkVersion}_${version.value}.jar"
  )
  .dependsOn(common)

lazy val `random-forest` = (project in file("odfactory/models/nlp/sentiment/ODFRandomForest"))
  .settings(
    version := "0.1.0",
    name := "random-forest",
    mainClass in assembly := Some("ODFRandomForest"),
    assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false),
    assemblyJarName in assembly := s"${name.value}_${scalaBinaryVersion.value}-${sparkVersion}_${version.value}.jar"
  )
  .dependsOn(common)

lazy val `support-vector-machine` = (project in file("odfactory/models/nlp/sentiment/ODFSupportVectorMachine"))
  .settings(
    version := "0.1.0",
    name := "support-vector-machine",
    mainClass in assembly := Some("ODFSupportVectorMachine"),
    assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false),
    assemblyJarName in assembly := s"${name.value}_${scalaBinaryVersion.value}-${sparkVersion}_${version.value}.jar"
  )
  .dependsOn(common)


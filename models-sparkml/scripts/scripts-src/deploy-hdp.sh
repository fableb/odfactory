#!/bin/sh
# ******************************************************************************
#
# Deploy data and trained models into Haddop FS.
#
# (C) 2019, Fabrice LEBEL, fabrice.lebel.pro@outlook.com
#
# ******************************************************************************

INSTALL_DIR=odfactory
DATA_DIR=$INSTALL_DIR/data
TRAINED_MODELS_DIR=$INSTALL_DIR/trained-models/sparkml
HADOOP_CMD="hdfs dfs"

if [ $# -ne 1 ]; then
    echo "USAGE: $0 user_name"
    exit 1
fi

# Set User
USER=$1

echo "Starting deployement for user '$USER'"
echo

# Remove old directories
echo "Removing old '$INSTALL_DIR'"
sudo -u hdfs $HADOOP_CMD -rm -r -skipTrash /$INSTALL_DIR
echo "Removing done."
echo

# Create new base directory
echo "Creating directory '$INSTALL_DIR' ..."
sudo -u hdfs $HADOOP_CMD -mkdir /$INSTALL_DIR
echo "Creating directory $INSTALL_DIR done."
echo

# Change owner to USER
echo "Changing owner for '$INSTALL_DIR' to '$USER' ..."
sudo -u hdfs $HADOOP_CMD -chown $USER /$INSTALL_DIR
echo "Changing owner to '$USER' done."
echo

# Copy files in data directory
echo "Copying '$DATA_DIR' ..."
$HADOOP_CMD -copyFromLocal ./data /$DATA_DIR
echo "Copying '$DATA_DIR' done."
echo

# Copy trained models directory files
echo "Copying '$TRAINED_MODELS_DIR' ..."
$HADOOP_CMD -copyFromLocal ./trained-models /$TRAINED_MODELS_DIR
echo "Copying '$TRAINED_MODELS_DIR' done."
echo

echo "Deployement done."


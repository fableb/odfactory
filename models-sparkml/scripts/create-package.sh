#!/bin/sh
# ******************************************************************************
#
# Script to create a package for local use or on Hortonworks-HDP platform.
#
# (C) 2019, Fabrice LEBEL, fabrice.lebel.pro@outlook.com
#
# ******************************************************************************

APP_VERSION="0.1.0"
SCALA_VERSION="2.11"
SPARK_VERSION="2.1.0"
SPARK_MEMORY_OPTION="--driver-memory 12g"

SBT_PGM="sbt"
# if [ ! -f $(SBT_PGM) ]; then
#     echo "ERROR: You must set the path to Scala SBT program ('sbt') or add it to your path"
#     exit 1
# fi

ARCHIVE_PGM=zip

SCRIPTS_SRC_DIR="./scripts-src"
DATASET_TRAIN_SRC_FILE="../data/train_dataset.csv"
DATASET_TEST_SRC_FILE="../data/test_dataset.csv"
APP_CONF_DEV_SRC_FILE="../sparkml-local.conf"
APP_CONF_HPD_SRC_FILE="../sparkml-hortonworks-hdp.conf"
DEPLOY_HDP_SRC_FILE="${SCRIPTS_SRC_DIR}/deploy-hdp.sh"
APP_JAR_SRC_FILE="../target/scala-2.11/models-sparkml_${SCALA_VERSION}-${SPARK_VERSION}_${APP_VERSION}.jar"
TRAINED_MODELS_SRC_DIR="../trained-models/sparkml"
TRAINED_MODELS="Spark-TFIDF Spark-LogisticRegressionModel Spark-RandomForestModel Spark-SupportVectorMachineModel"

PACKAGE_DEST_DIR="odfactory"
DATASETS_DEST_DIR="${PACKAGE_DEST_DIR}/data"
TRAINED_MODELS_DEST_DIR="${PACKAGE_DEST_DIR}/trained-models/sparkml"
APP_CONF_DEST_DIR="${PACKAGE_DEST_DIR}/config"

# Construction du package
echo "***********************"
echo "* Building package... *"
echo "***********************"
echo

echo "Building fat JAR file..."
cd ../ ; $SBT_PGM clean assembly
cd ./scripts/
echo "Building fat JAR file done."
echo

# Creation du dossier de packaging
if [ -d "${PACKAGE_DEST_DIR}" ]; then
    echo "Deleting old packaging directory..."
    rm -rf "${PACKAGE_DEST_DIR}"
    echo "Deleting done."
fi

echo "Creating packging directory..."
mkdir "${PACKAGE_DEST_DIR}"
echo "Packaging directory '${PACKAGE_DEST_DIR}' created."
echo

# Creation du dossier des fichiers de configuration
echo "Creating configuration files directory..."
mkdir "${APP_CONF_DEST_DIR}"
echo "Configuration files directory '${APP_CONF_DEST_DIR}' created."
echo

# Creation du dossier des datasets
echo "Creating datasets directory..."
mkdir ${DATASETS_DEST_DIR}
echo "Datasets directory '${DATASETS_DEST_DIR}' created."
echo

# Creation du dossier des modeles pre-entraines
echo "Creating models directory"
mkdir -p ${TRAINED_MODELS_DEST_DIR}
echo "Models directory '${TRAINED_MODELS_DEST_DIR}' created."
echo

# Copie des datasets
if [ -f ${DATASET_TRAIN_SRC_FILE} ]; then
    echo "Copying ${DATASET_TRAIN_SRC_FILE} file..."
    cp ${DATASET_TRAIN_SRC_FILE} ${DATASETS_DEST_DIR}
    echo "Copying ${DATASET_TRAIN_SRC_FILE} file done."
    echo
else
    echo "ERROR: ${DATASET_TRAIN_SRC_FILE} is missing."
    exit 1
fi

if [ -f ${DATASET_TEST_SRC_FILE} ]; then
    echo "Copying ${DATASET_TEST_SRC_FILE} file..."
    cp ${DATASET_TEST_SRC_FILE} ${DATASETS_DEST_DIR}
    echo "Copying ${DATASET_TEST_SRC_FILE} file done."
    echo
else
    echo "ERROR: ${DATASET_TEST_SRC_FILE} is missing."
    exit 1
fi

# Copie des modeles pre-entraines
for model in ${TRAINED_MODELS}; do
    echo "Copying model ${TRAINED_MODELS_SRC_DIR}/${model}..."
    if [ -d ${TRAINED_MODELS_SRC_DIR}/${model} ]; then
        cp -r "${TRAINED_MODELS_SRC_DIR}/${model}" ${TRAINED_MODELS_DEST_DIR}
        echo "Copying model ${TRAINED_MODELS_SRC_DIR}/${model} done."
    else
        echo "WARNING: Model ${TRAINED_MODELS_SRC_DIR}/${model} not available."
    fi
done
echo

# Copie du fichier JAR
if [ -f ${APP_JAR_SRC_FILE} ]; then
    echo "Copying ${APP_JAR_SRC_FILE} file..."
    cp ${APP_JAR_SRC_FILE} ${PACKAGE_DEST_DIR}
    echo "Copying ${APP_JAR_SRC_FILE} file done."
    echo
else
    echo "ERROR: JAR file ${APP_JAR_SRC_FILE} is missing."
    exit 1
fi

# Copie du script de deploiement Hadoop
echo "Copying file '${DEPLOY_HDP_SRC_FILE}' ..."
cp --force ${DEPLOY_HDP_SRC_FILE} "${PACKAGE_DEST_DIR}/"
echo "Copying file '${DEPLOY_HDP_SRC_FILE}' done."
echo


# Copie des fichiers de configuration
echo "Copying file '${APP_CONF_DEV_SRC_FILE}' ..."
cp --force ${APP_CONF_DEV_SRC_FILE} "${APP_CONF_DEST_DIR}/"
echo "Copying file '${APP_CONF_DEV_SRC_FILE}' done."

echo "Copying file '${APP_CONF_HPD_SRC_FILE}' ..."
cp --force ${APP_CONF_HPD_SRC_FILE} "${APP_CONF_DEST_DIR}/"
echo "Copying file '${APP_CONF_HPD_SRC_FILE}' done."
echo

# Creation du script d'entrainement des models
cat > "${PACKAGE_DEST_DIR}/train-models.sh" <<EOF 
#!/bin/sh
# ******************************************************************************
#
# Script to train models on distributed platform.
#
# (C) 2019, Fabrice LEBEL, fabrice.lebel.pro@outlook.com
#
# ******************************************************************************

CONFIG_FILE=\`echo \$1\`

if [ \$# -ne 1 ]; then
    echo "USAGE: \${0} [CONFIG_FILE]"
    echo "  [CONFIG_FILE]: Configuration file in 'config' directory"
    exit 1
fi

JAR_FILE=models-sparkml_${SCALA_VERSION}-${SPARK_VERSION}_${APP_VERSION}.jar
SPARK_DRIVER_MEMORY="${SPARK_MEMORY_OPTION}"

echo ">>> Training Logistic Regression..."
spark-submit \${SPARK_DRIVER_MEMORY} --class "odfactory.models.nlp.sentiment.LogisticRegressionTrain" "\${JAR_FILE}" "\${CONFIG_FILE}"
echo ">>> Training Logistic Regression done."
echo

echo ">>> Training Random Forest..."
spark-submit \${SPARK_DRIVER_MEMORY} --class "odfactory.models.nlp.sentiment.RandomForestTrain" "\${JAR_FILE}" "\${CONFIG_FILE}"
echo ">>> Training Random Forest done."
echo

echo ">>> Training Support Vector Machine..."
spark-submit \${SPARK_DRIVER_MEMORY} --class "odfactory.models.nlp.sentiment.SupportVectorMachineTrain" "\${JAR_FILE}" "\${CONFIG_FILE}"
echo ">>> Training Support Vector Machine done."
echo

EOF

# Creation de l'archive finale
PACKAGE_FILENAME="${PACKAGE_DEST_DIR}.zip"

if [ -f "${PACKAGE_FILENAME}" ]; then
    echo "Deleting old archive '${PACKAGE_FILENAME}'..."
    rm -f ${PACKAGE_FILENAME}
    echo "Deleting old archive '${PACKAGE_FILENAME}' done."
    echo
fi

if [ -d ${PACKAGE_DEST_DIR} ]; then
    echo "Creating package '${PACKAGE_FILENAME}'..."
    $ARCHIVE_PGM -r ${PACKAGE_FILENAME} ${PACKAGE_DEST_DIR}
    echo "Creating package '${PACKAGE_FILENAME}' done."
    echo
else
    echo "ERROR: Directory '${PACKAGE_DEST_DIR}' is missing. Cannot create package."
    exit 1
fi

echo "Building package done."
echo

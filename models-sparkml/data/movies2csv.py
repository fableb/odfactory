# -*- coding: utf-8 -*-
"""
Large Movie Reviw Dataset (http://ai.stanford.edu/~amaas/data/sentiment/)

Transform raw data to csv files.

(c) 2019, Fabrice LEBEL
"""
import codecs
from bs4 import BeautifulSoup
from os import listdir
from os.path import isfile, join


def treatFile(dirpath, filename):
    """

    :param dirpath:
    :param filename:
    :return:
    """
    f = codecs.open(dirpath + '/' + filename, encoding='utf-8', mode='r')
    res = filename.split('.')[0].split('_')
    res = res + f.readlines()
    f.close()
    return res


def createCSV(dirpath, outfilename, sentiment=''):
    """

    :param dirpath:
    :param outfilename:
    :param sentiment:
    :return:
    """
    files = [f for f in listdir(dirpath) if isfile(join(dirpath, f))]
    # print(files)

    # Create CSV file
    f = codecs.open(outfilename, encoding='utf-8', mode='w')
    f.write('id,rating,sentiment,raw_comment,clean_Comment\n')
    for file in files:
        tmp = treatFile(dirpath, file)
        raw_comment = tmp[2].replace('"', "").strip()  # Remove " from string
        clean_comment = BeautifulSoup(raw_comment, "lxml").text  # Remove HTML tags from string
        f.write(
            tmp[0] + '-' + sentiment + ',' + tmp[
                1] + ',' + sentiment + ',"' + raw_comment + '","' + clean_comment + '"\n')
    f.close()


def mergeCSV(outfilename, filename1, filename2):
    """
    Merge 2 CSV files into one CSV file

    :param outfilename: CSV output file name
    :param filename1: first CSV file name
    :param filename2: second CSV file name
    :return:
    """
    # Open output file
    fout = open(outfilename, "w")
    # First CSV file
    for line in open(filename1, "r"):
        fout.write(line)
    # Second CSV file
    f = open(filename2, "r")
    # f.next()  # Skip the header for Python 2.x
    f.__next__()  # Skip the header for Python 3.x
    for line in f:
        fout.write(line)
    f.close()
    fout.close()


def main():
    trainpath_neg = "./aclImdb/train/neg"
    trainpath_pos = "./aclImdb/train/pos"
    trainpath_unsup = "./aclImdb/train/unsup"
    testpath_neg = "./aclImdb/test/neg"
    testpath_pos = "./aclImdb/test/pos"

    print('> Create train dataset negative... ')
    createCSV(trainpath_neg, 'train_neg.csv', sentiment='neg')
    print('done\n')

    print('> Create train dataset positive... ')
    createCSV(trainpath_pos, 'train_pos.csv', sentiment='pos')
    print('done\n')

    print('> Create train dataset unsupervised... ')
    createCSV(trainpath_unsup, 'train_unsup.csv', sentiment='unsup')
    print('done\n')

    print('> Create test dataset negative... ')
    createCSV(testpath_neg, 'test_neg.csv', sentiment='neg')
    print('done\n')

    print('> Create test dataset positive... ')
    createCSV(testpath_pos, 'test_pos.csv', sentiment='pos')
    print('done\n')

    print('> Merge train datasets... ')
    mergeCSV('train_dataset.csv', 'train_pos.csv', 'train_neg.csv')
    print('done\n')

    print('> Merge test datasets... ')
    mergeCSV('test_dataset.csv', 'test_pos.csv', 'test_neg.csv')
    print('done\n')


if __name__ == "__main__":
    main()

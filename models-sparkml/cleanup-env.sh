#!/bin/sh
# ******************************************************************************
#
# Cleanup development environment.
#
# (C) 2019, Fabrice LEBEL, fabrice.lebel.pro@outlook.com
#
# ******************************************************************************

# -----------------------------------
# Cleanup Scala/SBT build directories
# -----------------------------------
rm -rf ./odfactory/

rm -rf ./spark-warehouse

rm -rf ./target

rm -rf ./trained-models

rm *.log

rm -rf ./project/META-INF/

rm -rf ./project/project/

rm -rf ./project/target/

rm project/build.properties

# --------------------------
# Cleanup 'script" directory
# --------------------------
rm -rf scripts/odfactory/

rm scripts/odfactory.zip

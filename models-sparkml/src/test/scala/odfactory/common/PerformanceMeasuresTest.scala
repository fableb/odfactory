package odfactory.common

import org.scalatest.FunSuite

/**
  * @author Fabrice LEBEL, fabrice.lebel.pro@outlook.com
  */
class PerformanceMeasuresTest extends FunSuite {

  test("PerformanceMeasures metrics normal case") {
    val perf: PerformanceMeasures = new PerformanceMeasures(2, 5, 7, 10)
    assert(perf.total === perf.trueNegative + perf.falseNegative + perf.falsePositive + perf.truePositive)
    assert(perf.accuracy === (perf.trueNegative + perf.truePositive) / perf.total)
    assert(perf.recall === perf.truePositive / (perf.falseNegative + perf.truePositive))
    assert(perf.specificity === perf.trueNegative / (perf.trueNegative + perf.falsePositive))
    assert(perf.precision === perf.truePositive / (perf.falsePositive + perf.truePositive))
    assert(perf.negativePredictiveValue === perf.trueNegative / (perf.trueNegative + perf.falseNegative))
  }

  test("PerformanceMeasures metrics: Divide by zero in computed measures") {
    val perf: PerformanceMeasures = new PerformanceMeasures(0, 0, 0, 0)
    assert(perf.total === 0)
    assert(perf.accuracy === 0)
    assert(perf.recall === 0)
    assert(perf.specificity === 0)
    assert(perf.precision === 0)
    assert(perf.negativePredictiveValue === 0)
  }
}

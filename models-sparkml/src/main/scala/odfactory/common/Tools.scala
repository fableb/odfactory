package odfactory.common

import java.io.InputStream
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import org.apache.spark.sql.{Dataset, Row, SparkSession}
import org.apache.spark.sql.functions.{col, udf}

import scala.reflect.io.File

/**
* @author Fabrice LEBEL, fabrice.lebel.pro@outlook.com
*/
class Tools {
  def getLinesToArray(path: String): Array[String] = {
    val stream: InputStream = getClass.getResourceAsStream(path)
    val lines = scala.io.Source.fromInputStream(stream).getLines.toArray
    lines
  }

  def createTimestamp(): String = {
    // Time stamp with the form "yyyy-mm-dd hh:mm;ss" (i.e. "2019-05-07 02:56:48")
    LocalDateTime.now.format(DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss"))
  }

  def deleteDirectory(directory: String): Unit = {
    val dir = File(directory)
    if (dir.isDirectory && dir.exists) {
      dir.deleteRecursively()
    }
  }
}

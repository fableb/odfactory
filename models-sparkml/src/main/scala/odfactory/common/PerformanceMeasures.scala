package odfactory.common

/**
  * @author Fabrice LEBEL, fabrice.lebel.pro@outlook.com
  */
class PerformanceMeasures(val trueNegative: Double, val falseNegative: Double, val falsePositive: Double, val truePositive: Double) {
  val total: Double = trueNegative + falseNegative + falsePositive + truePositive
  val accuracy: Double = if (total != 0) (trueNegative + truePositive) / total else 0
  val recall: Double = if (falseNegative + truePositive != 0) truePositive / (falseNegative + truePositive) else 0
  val specificity: Double = if (trueNegative + falsePositive != 0) trueNegative / (trueNegative + falsePositive) else 0
  val precision: Double = if (falsePositive + truePositive != 0) truePositive / (falsePositive + truePositive) else 0
  val negativePredictiveValue: Double = if (trueNegative + falseNegative != 0) trueNegative / (trueNegative + falseNegative) else 0

  def print: Unit = {
    println("***** Performance Measures *****")
    println(" True Negative: " + trueNegative.toInt)
    println("False Negative: " + falseNegative.toInt)
    println("False Positive: " + falsePositive.toInt)
    println(" True Positive: " + truePositive.toInt)
    println()
    println("                   Accuracy: " + accuracy)
    println("        Sensitivity, Recall: " + recall)
    println("                Specificity: " + specificity)
    println("Pos. pred. value, Precision: " + precision)
    println("           Neg. pred. value: " + negativePredictiveValue)
    println()
  }
}

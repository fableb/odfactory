package odfactory.models.nlp.sentiment

import java.io.File

import com.typesafe.config.{Config, ConfigFactory}
import odfactory.common.Tools
import org.apache.spark.ml.{Pipeline, PipelineModel}
import org.apache.spark.ml.feature.{CountVectorizer, IDF, StopWordsRemover, Tokenizer}
import org.apache.spark.sql.functions.{col, udf}
import org.apache.spark.sql.{Dataset, Row, SparkSession}
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}

/**
  * @author Fabrice LEBEL, fabrice.lebel.pro@outlook.com
  */
class ODFTfIdf(sparkSession: SparkSession) {

  def loadCSVDataSet(path: String): Dataset[Row] = {
    val csvSchema = StructType(Array(
      StructField("id", StringType, nullable = true),
      StructField("target", IntegerType, nullable = true),
      StructField("target_label", StringType, nullable = true), // remove
      StructField("text", StringType, nullable = true),
      StructField("clean_text", StringType, nullable = true) // remove
    ))

    val targetEncoder = udf((target: Int) => {
      if (target > 5) 1
      else 0
    })

    val cleanText = udf((text: String) => {
      text.toLowerCase()
        .replaceAll("[^a-zA-Z\\s]", "")
    })

    val dataFrame: Dataset[Row] = sparkSession.read.format("csv")
      .option("header", "true")
      .schema(csvSchema)
      .load(path)

    dataFrame
      .withColumn("target", targetEncoder(col("target")))
      .withColumn("text", cleanText(col("text")))
      .select("id", "target", "text")
  }

  def createTrainTFIDFPipeline(): Pipeline = {
    val tokenizerStage: Tokenizer = new Tokenizer()
      .setInputCol("text")
      .setOutputCol("words")

    val stopWords: Array[String] = (new Tools).getLinesToArray("/stopwords.txt")
    val stopWordsRemoverStage: StopWordsRemover = new StopWordsRemover()
      .setStopWords(stopWords)
      .setCaseSensitive(false)
      .setInputCol("words")
      .setOutputCol("words_filtered")

    val tfStage: CountVectorizer = new CountVectorizer()
      .setMinTF(1.0)
      .setMinDF(5.0)
      .setInputCol("words_filtered")
      .setOutputCol("tf")

    val idfStage: IDF = new IDF()
      .setInputCol("tf")
      .setOutputCol("tfidf")

    // Create feature pipeline
    val pipeline = new Pipeline()
      .setStages(Array(tokenizerStage, stopWordsRemoverStage, tfStage, idfStage))
    pipeline
  }
}

object ODFTfIdf {
  private val APP_NAME: String = "TF-IDF"

  def main(args: Array[String]): Unit = {
    println("Load configuration file '" + args(0) + "'")
    val config: Config = ConfigFactory.parseFile(new File(args(0)))

    val sparkSession: SparkSession = SparkSession
      .builder
      .appName(APP_NAME)
      .master(config.getString("appConf.spark.master"))
      .getOrCreate()
    sparkSession.sparkContext.setLogLevel("ERROR")

    val tfidfModel = new ODFTfIdf(sparkSession)

    // Load data sets
    val dataSetPath = config.getString("appConf.datasetsPath")

    val trainDataSetPath = dataSetPath + "/train_dataset.csv"
    val trainDF: Dataset[Row] = tfidfModel.loadCSVDataSet(trainDataSetPath)

    val testDataSetPath = dataSetPath + "/test_dataset.csv"
    val testDF: Dataset[Row] = tfidfModel.loadCSVDataSet(testDataSetPath)

    // Create TF-IDF model on train data set and save it
    val pipeline: Pipeline = tfidfModel.createTrainTFIDFPipeline()
    val pipelineModel: PipelineModel = pipeline.fit(trainDF)
    val modelsPath = config.getString("appConf.modelsPath")
    pipelineModel.write.overwrite().save(modelsPath + "/Spark-TFIDF")

    // Apply TF-IDF model on train and test data sets
    val trainTFIDF: Dataset[Row] = pipelineModel.transform(trainDF)
    trainTFIDF.printSchema()
    trainTFIDF.take(5).foreach(println)
    println()
    val testTFIDF: Dataset[Row] = pipelineModel.transform(testDF)
    testTFIDF.printSchema()
    testTFIDF.take(5).foreach(println)
  }
}

package odfactory.models.nlp.sentiment

import java.io.File

import com.typesafe.config.{Config, ConfigFactory}
import odfactory.common.PerformanceMeasures
import org.apache.spark.ml.PipelineModel
import org.apache.spark.ml.classification.{LogisticRegression, LogisticRegressionModel}
import org.apache.spark.ml.evaluation.BinaryClassificationEvaluator
import org.apache.spark.ml.linalg.DenseVector
import org.apache.spark.sql.functions.{col, udf}
import org.apache.spark.sql.{Dataset, Row, SparkSession}

/**
  * @author Fabrice LEBEL, fabrice.lebel.pro@outlook.com
  */
class ODFLogisticRegression(sparkSession: SparkSession) {
  var logisticRegressionModel: LogisticRegressionModel = _

  def train(data: Dataset[Row]): Unit = {
    val logisticRegression = new LogisticRegression()
      .setFamily("multinomial")
      .setLabelCol("target")
      .setFeaturesCol("tfidf")
      .setMaxIter(300)
      .setRegParam(0.001)
      .setThreshold(0.5)
    logisticRegressionModel = logisticRegression.fit(data)
  }

  def predict(data: Dataset[Row]): Dataset[Row] = {
    val predictions: Dataset[Row] = logisticRegressionModel.transform(data)
    predictions
  }

  def getPerformanceMeasures(predictions: Dataset[Row]): PerformanceMeasures = {
    import sparkSession.implicits._ // filter, select, toDF....

    val trueNegative = predictions.filter($"prediction" === 0.0)
      .filter($"target" === $"prediction").count()
    val truePositive = predictions.filter($"prediction" === 1.0)
      .filter($"target" === $"prediction").count()
    val falseNegative = predictions.filter($"prediction" === 0.0)
      .filter($"target" =!= $"prediction").count()
    val falsePositive = predictions.filter($"prediction" === 1.0)
      .filter($"target" =!= $"prediction").count()
    new PerformanceMeasures(trueNegative, falseNegative, falsePositive, truePositive)
  }

  def getAROC(predictions: Dataset[Row]): Double = {
    val evaluatorROC = new BinaryClassificationEvaluator()
      .setLabelCol("target")
      .setRawPredictionCol("probability")
      .setMetricName("areaUnderROC")
    evaluatorROC.evaluate(predictions)
  }
}

object ODFLogisticRegression {
  private val APP_NAME: String = "LogisticRegressionClassifier"
  private implicit val config: Config = ConfigFactory.load()

  def main(args: Array[String]): Unit = {
    println("Load configuration file '" + args(0) + "'")
    val config = ConfigFactory.parseFile(new File(args(0)))

    val sparkSession = SparkSession
      .builder
      .appName(APP_NAME)
      .master(config.getString("appConf.spark.master"))
      .getOrCreate()
    sparkSession.sparkContext.setLogLevel("ERROR")

    val tfidfModel: ODFTfIdf = new ODFTfIdf(sparkSession)

    // Load data sets
    val dataSetsPath = config.getString("appConf.datasetsPath")
    val train: Dataset[Row] = tfidfModel.loadCSVDataSet(dataSetsPath + "/train_dataset.csv")
    val test: Dataset[Row] = tfidfModel.loadCSVDataSet(dataSetsPath + "/test_dataset.csv")

    // Load trained TF-IDF model
    val modelsPath = config.getString("appConf.modelsPath")
    val tfidfPipeline: PipelineModel = PipelineModel.load(modelsPath + "/Spark-TFIDF")

    // Apply trained TF-IDF model on train and test data sets
    val trainTFIDF: Dataset[Row] = tfidfPipeline.transform(train)
    val testTFIDF: Dataset[Row] = tfidfPipeline.transform(test)

    // Machine Learning
    val odfLogisticRegression: ODFLogisticRegression = new ODFLogisticRegression(sparkSession)
    odfLogisticRegression.train(trainTFIDF)
    val trainPredictions: Dataset[Row] = odfLogisticRegression.predict(trainTFIDF)
    trainPredictions.cache()
    val testPredictions: Dataset[Row] = odfLogisticRegression.predict(testTFIDF)
    testPredictions.cache()

    // Performance measures on data sets
    val trainPerformanceMeasures: PerformanceMeasures = odfLogisticRegression.getPerformanceMeasures(trainPredictions)
    trainPerformanceMeasures.print
    val testPerformanceMeasures: PerformanceMeasures = odfLogisticRegression.getPerformanceMeasures(testPredictions)
    testPerformanceMeasures.print
    println("Area under ROC = " + odfLogisticRegression.getAROC(testPredictions))

    // Save trained model
    odfLogisticRegression.logisticRegressionModel.write.overwrite().save(modelsPath + "/Spark-LogisticRegressionModel")

    // *************************************************************************
    // Example without target column
    // *************************************************************************
    val savedLogisticRegression = LogisticRegressionModel.load(modelsPath + "/Spark-LogisticRegressionModel")

    val cleanText = udf((text: String) => {
      text.toLowerCase()
        .replaceAll("[^a-zA-Z\\s]", "")
    })

    val unlabeled = sparkSession.createDataFrame(Seq(
      (1L, "Star Wars 2 was an amazing movie. You must see it"),
      (7L, "This is a very bad movie. Don't loose your money!")
    )).toDF("id", "text")
      .withColumn("text", cleanText(col("text")))
      .select("id", "text")
    unlabeled.show()

    // TF-IDF matrix
    val unlabeledFitted = tfidfPipeline.transform(unlabeled)
    unlabeledFitted.show()

    // Predictions
    val predictions = savedLogisticRegression.transform(unlabeledFitted)
    predictions.show()
    predictions.select("id", "text", "probability", "prediction")
      .collect()
      .foreach { case Row(id: Long, comment: String, prob: DenseVector, prediction: Double) =>
        println(s"($id, $comment) --> prob=$prob, prediction=$prediction")
      }

    sparkSession.close()
  }
}

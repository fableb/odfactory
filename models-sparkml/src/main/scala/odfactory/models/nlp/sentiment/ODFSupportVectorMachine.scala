package odfactory.models.nlp.sentiment

import java.io.File

import com.typesafe.config.{Config, ConfigFactory}
import odfactory.common.{PerformanceMeasures, Tools}
import org.apache.spark.ml.PipelineModel
import org.apache.spark.mllib.classification.{SVMModel, SVMWithSGD}
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.functions.{col, udf}
import org.apache.spark.sql.{Dataset, Row, SparkSession}

/**
  * @author Fabrice LEBEL, fabrice.lebel.pro@outlook.com
  */
object ODFSupportVectorMachine {
  private val APP_NAME: String = "SupportVectorMachineClassifier"
  private implicit val config: Config = ConfigFactory.load()

  def main(args: Array[String]): Unit = {
    println("Load configuration file '" + args(0) + "'")
    val config = ConfigFactory.parseFile(new File(args(0)))

    val sparkSession = SparkSession
      .builder
      .appName(APP_NAME)
      .master(config.getString("appConf.spark.master"))
      .getOrCreate()
    sparkSession.sparkContext.setLogLevel("ERROR")

    val tfidfModel: ODFTfIdf = new ODFTfIdf(sparkSession)

    // Load data sets
    val dataSetsPath = config.getString("appConf.datasetsPath")
    val train: Dataset[Row] = tfidfModel.loadCSVDataSet(dataSetsPath + "/train_dataset.csv")
    val test: Dataset[Row] = tfidfModel.loadCSVDataSet(dataSetsPath + "/test_dataset.csv")

    // Load trained TF-IDF model
    val modelsPath = config.getString("appConf.modelsPath")
    val tfidfPipeline: PipelineModel = PipelineModel.load(modelsPath + "/Spark-TFIDF")

    // Apply trained TF-IDF model on train and test data sets
    val trainTFIDF: Dataset[Row] = tfidfPipeline.transform(train)
    val testTFIDF: Dataset[Row] = tfidfPipeline.transform(test)

    // Machine learning
    val trainTFIDFRdd: RDD[LabeledPoint] = trainTFIDF.rdd.map(row => LabeledPoint(
      row.getAs[Int]("target"),
      org.apache.spark.mllib.linalg.Vectors.fromML(row.getAs[org.apache.spark.ml.linalg.SparseVector]("tfidf")))
    )

    val svm = SVMWithSGD.train(trainTFIDFRdd, 100)

    val testTFIDFRdd: RDD[LabeledPoint] = testTFIDF.rdd.map(row => LabeledPoint(
      row.getAs[Int]("target"),
      org.apache.spark.mllib.linalg.Vectors.fromML(row.getAs[org.apache.spark.ml.linalg.SparseVector]("tfidf")))
    )
    testTFIDFRdd.cache()

    // Performances
    val predictionAndLabels = testTFIDFRdd.map { point =>
      val prediction = svm.predict(point.features)
      (prediction, point.label)
    }

    // Target = 0 and prediction = target
    val trueNegative = predictionAndLabels.filter(x => x._2 == 0).filter(x => x._2 == x._1).count()
    // Target = 1 and prediction = target
    val truePositive = predictionAndLabels.filter(x => x._2 == 1.0).filter(x => x._2 == x._1).count()
    // Target = 0 and prediction != target
    val falseNegative = predictionAndLabels.filter(x => x._2 == 0.0).filter(x => x._2 != x._1).count()
    // Target = 1 and prediction != target
    val falsePositive = predictionAndLabels.filter(x => x._2 == 1.0).filter(x => x._2 != x._1).count()
    new PerformanceMeasures(trueNegative, falseNegative, falsePositive, truePositive).print

    // Other metrics
    val metrics = new BinaryClassificationMetrics(predictionAndLabels)
    val auROC = metrics.areaUnderROC()
    println(s"Area under ROC = $auROC")

    // Save SVM model
    val tools = new Tools
    tools.deleteDirectory(modelsPath + "/Spark-SupportVectorMachineModel")
    svm.save(sparkSession.sparkContext, modelsPath + "/Spark-SupportVectorMachineModel")

    // *************************************************************************
    // Example without target column
    // *************************************************************************
    val savedSVM = SVMModel.load(sparkSession.sparkContext, modelsPath + "/Spark-SupportVectorMachineModel")

    val cleanText = udf((text: String) => {
      text.toLowerCase()
        .replaceAll("[^a-zA-Z\\s]", "")
    })

    val unlabeled = sparkSession.createDataFrame(Seq(
      (1L, "Star Wars 2 was an amazing movie. You must see it"),
      (7L, "This is a very bad movie. Don't loose your money!")
    )).toDF("id", "text")
      .withColumn("text", cleanText(col("text")))
      .select("id", "text")
    unlabeled.show()

    // Create TF-IDF matrix
    val unlabeledFitted = tfidfPipeline.transform(unlabeled)
    unlabeledFitted.show()

    // Predictions
    val unlabeledFittedRDD = unlabeledFitted.rdd.map(row =>
      org.apache.spark.mllib.linalg.Vectors.fromML(row.getAs[org.apache.spark.ml.linalg.SparseVector]("tfidf"))
    )

    val unlabeledPredictionsAndLabels = unlabeledFittedRDD.map { point =>
      val prediction = savedSVM.predict(point)
      (prediction, point)
    }

    unlabeledPredictionsAndLabels.collect().foreach(println)
  }
}

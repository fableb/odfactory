package odfactory.models.nlp.sentiment

import java.io.File

import com.typesafe.config.{Config, ConfigFactory}
import odfactory.common.PerformanceMeasures
import org.apache.spark.ml.PipelineModel
import org.apache.spark.ml.classification.{RandomForestClassificationModel, RandomForestClassifier}
import org.apache.spark.ml.linalg.DenseVector
import org.apache.spark.sql.functions.{col, udf}
import org.apache.spark.sql.{Dataset, Row, SparkSession}

/**
  * @author Fabrice LEBEL, fabrice.lebel.pro@outlook.com
  */
object ODFRandomForest {
  private val APP_NAME: String = "RandomForestClassifier"
  private implicit val config: Config = ConfigFactory.load()

  def main(args: Array[String]): Unit = {
    println("Load configuration file '" + args(0) + "'")
    val config = ConfigFactory.parseFile(new File(args(0)))

    val sparkSession = SparkSession
      .builder
      .appName(APP_NAME)
      .master(config.getString("appConf.spark.master"))
      .getOrCreate()
    import sparkSession.implicits._ // filter, select, toDF....
    sparkSession.sparkContext.setLogLevel("ERROR")

    val tfidfModel: ODFTfIdf = new ODFTfIdf(sparkSession)

    // Load data sets
    val dataSetsPath = config.getString("appConf.datasetsPath")
    val train: Dataset[Row] = tfidfModel.loadCSVDataSet(dataSetsPath + "/train_dataset.csv")
    val test: Dataset[Row] = tfidfModel.loadCSVDataSet(dataSetsPath + "/test_dataset.csv")

    // Load trained TF-IDF model
    val modelsPath = config.getString("appConf.modelsPath")
    val tfidfPipeline: PipelineModel = PipelineModel.load(modelsPath + "/Spark-TFIDF")

    // Apply trained TF-IDF model on train and test data sets
    val trainTFIDF: Dataset[Row] = tfidfPipeline.transform(train)
    val testTFIDF: Dataset[Row] = tfidfPipeline.transform(test)

    // Machine learning
    val randomForestClassifier = new RandomForestClassifier()
      .setLabelCol("target")
      .setFeaturesCol("tfidf")
      .setNumTrees(600)

    val randomForestModel = randomForestClassifier.fit(trainTFIDF)
    val trainPredictions: Dataset[Row] = randomForestModel.transform(trainTFIDF)
    trainPredictions.cache()
    val testPredictions: Dataset[Row] = randomForestModel.transform(testTFIDF)
    testPredictions.cache()

    // Performance measures on test data set
    val trueNegative = testPredictions.filter($"prediction" === 0.0)
      .filter($"target" === $"prediction").count()
    val truePositive = testPredictions.filter($"prediction" === 1.0)
      .filter($"target" === $"prediction").count()
    val falseNegative = testPredictions.filter($"prediction" === 0.0)
      .filter($"target" =!= $"prediction").count()
    val falsePositive = testPredictions.filter($"prediction" === 1.0)
      .filter($"target" =!= $"prediction").count()
    new PerformanceMeasures(trueNegative, falseNegative, falsePositive, truePositive).print

    // Save trained model
    randomForestModel.write.overwrite().save(modelsPath + "/Spark-RandomForestModel")

    // *************************************************************************
    // Example without target column
    // *************************************************************************
    val savedRF = RandomForestClassificationModel.load(modelsPath + "/Spark-RandomForestModel")

    val cleanText = udf((text: String) => {
      text.toLowerCase()
        .replaceAll("[^a-zA-Z\\s]", "")
    })

    val unlabeled = sparkSession.createDataFrame(Seq(
      (1L, "Star Wars 2 was an amazing movie. You must see it"),
      (7L, "This is a very bad movie. Don't loose your money!")
    )).toDF("id", "text")
      .withColumn("text", cleanText(col("text")))
      .select("id", "text")
    unlabeled.show()

    // TF-IDF matrix
    val unlabeledFitted = tfidfPipeline.transform(unlabeled)
    unlabeledFitted.show()

    // Predictions
    val predictions = savedRF.transform(unlabeledFitted)
    //predictions.show()
    predictions.select("id", "text", "probability", "prediction")
      .collect()
      .foreach { case Row(id: Long, comment: String, prob: DenseVector, prediction: Double) =>
        println(s"($id, $comment) --> prob=$prob, prediction=$prediction")
      }
  }
}

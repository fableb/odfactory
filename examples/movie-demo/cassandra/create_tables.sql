/* ************************************************************************** */
/*                                                                            */
/* Tables creation script and data sample                                     */
/*                                                                            */
/* (C) 2019, Fabrice LEBEL. fabrice.lebel.pro@outlook.com                     */
/*                                                                            */
/* ************************************************************************** */
CREATE KEYSPACE sentiment_analysis
WITH REPLICATION = {
  'class': 'SimpleStrategy',
  'replication_factor' : 1
}; 

USE sentiment_analysis;

/* *********** */
/* Movie table */
/* *********** */
DROP TABLE IF EXISTS sentiment_analysis.movie;
CREATE TABLE sentiment_analysis.movie (
  movie_id TIMEUUID,
  movie_name TEXT,
  description TEXT,
  year int,
  PRIMARY KEY ((movie_id), movie_name)
) WITH CLUSTERING ORDER BY (movie_name ASC);

/* ******************************************** */
/* Comments received from Apache Kafka messages */
/* ******************************************** */
DROP TABLE IF EXISTS sentiment_analysis.kafka_comments;
CREATE TABLE sentiment_analysis.kafka_comments (
  comment_id TIMEUUID,
  movie_name TEXT,
  movie_id TIMEUUID,
  movie_comment TEXT,
  datetime TEXT,
  sentiment_value INT,
  sentiment_label TEXT,
  algorithms TEXT,
  PRIMARY KEY ((movie_name, movie_id), comment_id)
) WITH CLUSTERING ORDER BY (comment_id DESC);
CREATE INDEX sentval ON sentiment_analysis.kafka_comments (sentiment_value);

/* ************************* */
/* Word count for each movie */
/* ************************* */
DROP TABLE IF EXISTS sentiment_analysis.movie_wordcount;
CREATE TABLE sentiment_analysis.movie_wordcount (
  movie_name TEXT,
  movie_id TIMEUUID,
  word TEXT,
  word_frequency INT,
  datetime TEXT,
  PRIMARY KEY ((movie_name, movie_id), word_frequency, word)
) WITH CLUSTERING ORDER BY (word_frequency DESC, word ASC);

/* -------------------------------------------------------------------------- */
/* Movie table data                                                           */
/* -------------------------------------------------------------------------- */
/* The Matrix */
INSERT INTO sentiment_analysis.movie  (movie_id, movie_name, description, year) 
VALUES (dd708590-7059-11e9-8eec-bb51378f83a7, 'The Matrix', 'A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.', 1999) IF NOT EXISTS;

/* Game of Thrones Season 8 Episode 3 */
INSERT INTO sentiment_analysis.movie  (movie_id, movie_name, description, year) 
VALUES (8d5bdd50-7060-11e9-8eec-bb51378f83a7, 'Game of Thrones Season 8 Episode 3 – ''The Long Night''', 'The Night King and his army have arrived at Winterfell and the great battle begins. ', 2019) IF NOT EXISTS;

/* Tomb Raider 2018 */
INSERT INTO sentiment_analysis.movie  (movie_id, movie_name, description, year) 
VALUES (729f1890-7422-11e9-9ae5-9116fc548b6b, 'Tomb Raider', 'Lara Croft, the fiercely independent daughter of a missing adventurer, must push herself beyond her limits when she discovers the island where her father disappeared.
Director: Roar Uthaug
Writers: Geneva Robertson-Dworet (screenplay by), Alastair Siddons (screenplay by)...
Stars: Alicia Vikander, Dominic West, Walton Goggins...', 2018) IF NOT EXISTS;

/* -------------------------------------------------------------------------- */
/* Movie comments data                                                        */
/* -------------------------------------------------------------------------- */
/* The Matrix */
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'The Matrix',dd708590-7059-11e9-8eec-bb51378f83a7,'The Matrix is one of the best sci-fi movies ever made! I read it used to be one of Tarantino''s favourite ones too, but the second and third ones ruined the whole beautiful idea and brought the overall value of the movie pretty low. The story/philosophy is mind-blowing and the directing, special effects and the overall performance absolutely amazing.','2019-05-07 01:54:29',1,'pos','logistic-regression:1,random-forest:1,support-vector-machine:1');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'The Matrix',dd708590-7059-11e9-8eec-bb51378f83a7,'Right there with Seven and Silence of the Lambs for me. I love this film, and rate it a notch above some of the other great action films of our time (i.e., Terminator, Predator) because it has a really well thought out (although moderately unbelievable) story line. A lot of thought provoking material, and some interesting subtleties. I''ve seen it 10 times (I don''t usually watch a movie more than once), and I''ll watch it a few more. Enjoy!','2019-05-07 02:00:56',1,'pos','logistic-regression:1,random-forest:1,support-vector-machine:1');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'The Matrix',dd708590-7059-11e9-8eec-bb51378f83a7,'An excellent movie with tons of special effects, fight scenes. A really good film with an interesting story.','2019-05-07 02:02:59',1,'pos','logistic-regression:1,random-forest:1,support-vector-machine:1');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'The Matrix',dd708590-7059-11e9-8eec-bb51378f83a7,'This film has no saving graces. A silly, unbelievable plot is made worse by childish characterisation. The gratuitous violence at the end, with innocent security guards being shot to pieces by our illustrious "heroes" completes the film''s moral bankrupcy. Would someone tell me why the bad guys where dark glasses?','2019-05-07 02:04:56',0,'neg','logistic-regression:0,random-forest:1,support-vector-machine:0');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'The Matrix',dd708590-7059-11e9-8eec-bb51378f83a7,'Without a doubt one of the best and most influential movies of all time, the Matrix is the defining science fiction film of the 1990''s and the biggest leap the genre has taken since Stanley Kubrick''s 2001: a Space Odyssey and Ridley Scott''s Blade Runner. The Matrix is a ground-breaking motion picture that not only raised the bar for all the science-fiction films to come after it but also redefined the action genre with its thrilling action sequences and revolutionary visual effects.','2019-05-10 07:59:39',1,'pos','logistic-regression:1,random-forest:1,support-vector-machine:1');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'The Matrix',dd708590-7059-11e9-8eec-bb51378f83a7,'Even with the new 4k hdr these the graphics and action scenes area holding up to the test of time. It not only looks better than mainstream movies today, you''re hard pressed to tell what was added in visual effects and what was happening at the time of the shoot.  The entire tower scene, not just the bottom level, is a thing of beauty. From all of the rain and explosions and bits flying around that you can see clearly now in 4K, to the generated explosion when the bomb hits the basement. Just gorgeous. Give it a watch, or a rewatch. It''s worth it even now more than ever.','2019-05-10 08:00:55',1,'pos','logistic-regression:1,random-forest:1,support-vector-machine:0');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'The Matrix',dd708590-7059-11e9-8eec-bb51378f83a7,'By the end of the 90s, there hadn''t been much in terms of fresh, new sci-fi that we hadn''t seen before. Or so I thought.  The Matrix combined the best of hard science-fiction with Asian cinema''s frenetic and masterfully choreographed action sequences, gorgeous direction, pitch-perfect casting, and the best martial arts I''d ever seen in a Hollywood film.  I don''t think the sequels did it justice, but The Matrix - as a stand-alone film - remains one of my all time favorites in ANY genre.','2019-05-10 08:02:24',1,'pos','logistic-regression:1,random-forest:1,support-vector-machine:1');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'The Matrix',dd708590-7059-11e9-8eec-bb51378f83a7,'The Wachowski brothers really did excel themselves with this movie. It''s a brilliant movie on a number of different levels - the directing is excellent, the camera work is great, the visuals are stunning, the kung-fu is A+, acting is executed with style and conviction, and the plot is truly inspired. It''s really hard to use enough superlatives on this movie!','2019-05-10 08:05:34',1,'pos','logistic-regression:1,random-forest:1,support-vector-machine:1');

/* Game of Thrones Season 8 Episode 3 */
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Game of Thrones Season 8 Episode 3 – ''The Long Night''',8d5bdd50-7060-11e9-8eec-bb51378f83a7,'Music: 10/10 Visuals: 10/10 Story: 2/10 It wasn''t just a poorly written Episode. It destroyed everything that was built up in the last 8 years.','2019-05-07 02:56:48',0,'neg','logistic-regression:0,random-forest:1,support-vector-machine:0');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Game of Thrones Season 8 Episode 3 – ''The Long Night''',8d5bdd50-7060-11e9-8eec-bb51378f83a7,'While I enjoyed the episode as I saw it, despite being so dark, those feelings immediately changed as soon as it ended and I felt like this wasn''t the GoT I''ve come to love over the years. It pains me to say that I can''t bring myself to be excited for the next episode. Considering the disappointing end to this whole White Walker part, I have a feeling that DnD will disappoint with the ending of the show itself as well.','2019-05-07 03:06:33',1,'pos','logistic-regression:1,random-forest:1,support-vector-machine:1');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Game of Thrones Season 8 Episode 3 – ''The Long Night''',8d5bdd50-7060-11e9-8eec-bb51378f83a7,'Okay. If this is not the worst, it is one of the worst episode of GOT. I can''t believe that I loved this show. GOT used to be great but now it is just another fan servicing SHOW. I rated 4/10 for old times'' sake.','2019-05-07 03:11:06',0,'neg','logistic-regression:0,random-forest:1,support-vector-machine:0');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Game of Thrones Season 8 Episode 3 – ''The Long Night''',8d5bdd50-7060-11e9-8eec-bb51378f83a7,'Thanks for ruining a show that lasted DECADE in a single episode! I bet mouthbreathers will get their     happy ending with Dany and Jon kissing each other in the throne room','2019-05-07 03:17:41',1,'pos','logistic-regression:1,random-forest:1,support-vector-machine:1');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Game of Thrones Season 8 Episode 3 – ''The Long Night''',8d5bdd50-7060-11e9-8eec-bb51378f83a7,'People giving this a 10 out 10 are the reason why this episode turned out as it did. Could have been far better IMHO.  Beside the many plot holes and unlogical survivals, it was an overall descent filmed battle scene with great CGI and action.  The plot was underwhelming. It is clear that the show writers don''t have the guts to kill off the main characters. This reminds me of the walking dead, where, years ago, they started showing the same issues and now the show is getting worse every episode.  On the other hand they showed their guts by killing off the NK... Which is a bad decision.  I rate it 6 for that awesome battle scene. The plot was far from good.','2019-05-07 03:31:57',1,'pos','logistic-regression:1,random-forest:1,support-vector-machine:0');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Game of Thrones Season 8 Episode 3 – ''The Long Night''',8d5bdd50-7060-11e9-8eec-bb51378f83a7,'When a character can tell a better story than the actual show writers SMH. Complete waste of 8 years of build up and storytelling. So Disappointing.','2019-05-09 23:47:16',0,'neg','logistic-regression:0,random-forest:0,support-vector-machine:0');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Game of Thrones Season 8 Episode 3 – ''The Long Night''',8d5bdd50-7060-11e9-8eec-bb51378f83a7,'8 years for nothing. All this build up and hype for nothing. D&D knows nothing.','2019-05-09 23:49:20',0,'neg','logistic-regression:0,random-forest:1,support-vector-machine:0');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Game of Thrones Season 8 Episode 3 – ''The Long Night''',8d5bdd50-7060-11e9-8eec-bb51378f83a7,'While I enjoyed the episode as I saw it, despite being so dark, those feelings immediately changed as soon as it ended and I felt like this wasn''t the GoT I''ve come to love over the years. It pains me to say that I can''t bring myself to be excited for the next episode.  Considering the disappointing end to this whole White Walker part, I have a feeling that DnD will disappoint with the ending of the show itself as well.','2019-05-09 23:50:27',1,'pos','logistic-regression:1,random-forest:1,support-vector-machine:1');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Game of Thrones Season 8 Episode 3 – ''The Long Night''',8d5bdd50-7060-11e9-8eec-bb51378f83a7,'Firstly, massive credit to the music composer for this episode, the music was absolutely perfect, but that was about it. The visuals were great! But only on second viewing when having playback in 4k whilst also having the brightness on maximum, nobody could see what was going on at all throughout the episode if it wasn''t in 4k quality, which is a horrible experience. The writing was as if they got a 9 year old with no knowledge of game of thrones to write it, the army tactics were absolutely tragic and the amount of plot armour is actually unreal, screw the final battle against Cercie, just let Jon Snow run at the Lannister army all on his own, his plot armour will save him.  Summarising, this was a gigantic middle finger to every single fan.','2019-05-09 23:52:31',1,'pos','logistic-regression:1,random-forest:1,support-vector-machine:0');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Game of Thrones Season 8 Episode 3 – ''The Long Night''',8d5bdd50-7060-11e9-8eec-bb51378f83a7,'I grew up watching this show, I waited year after year with a lot of enthusiasm for every season, and after this episode I''m so disappointed. I''m not curious what will happen next.','2019-05-09 23:54:01',1,'pos','logistic-regression:1,random-forest:1,support-vector-machine:1');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Game of Thrones Season 8 Episode 3 – ''The Long Night''',8d5bdd50-7060-11e9-8eec-bb51378f83a7,'Okay. If this is not the worst, it is one of the worst episode of GOT. I can''t believe that I loved this show. GOT used to be great but now it is just another fan servicing SHOW. I rated 4/10 for old times'' sake.','2019-05-09 23:56:17',0,'neg','logistic-regression:0,random-forest:1,support-vector-machine:0');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Game of Thrones Season 8 Episode 3 – ''The Long Night''',8d5bdd50-7060-11e9-8eec-bb51378f83a7,'The worst episode in GOT history. 4 points only for epic shots.','2019-05-09 23:57:17',0,'neg','logistic-regression:0,random-forest:1,support-vector-machine:0');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Game of Thrones Season 8 Episode 3 – ''The Long Night''',8d5bdd50-7060-11e9-8eec-bb51378f83a7,'This is not GoT TV show I loved. Still entertaining, but dumb as any other average tv show from the 90s. I think HBO writers can''t be fully adult people. Just fanboy kids who never understood what was the all tv show about. Plot armors, teleports, stupid storytelling, characters uselessness... things could never be approved by GRRM!','2019-05-09 23:58:28',1,'pos','logistic-regression:1,random-forest:0,support-vector-machine:1');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Game of Thrones Season 8 Episode 3 – ''The Long Night''',8d5bdd50-7060-11e9-8eec-bb51378f83a7,'For something that was hyped up since season 1. This Episode was pathetic. Night King deserved better. So did we.','2019-05-09 23:59:26',0,'neg','logistic-regression:0,random-forest:1,support-vector-machine:0');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Game of Thrones Season 8 Episode 3 – ''The Long Night''',8d5bdd50-7060-11e9-8eec-bb51378f83a7,'This is not GOT, it''s just another Hollywood fantasy in which all main characters survive.  GOT used to be Shakespearean, now it''s just Bayan...','2019-05-10 00:00:58',0,'neg','logistic-regression:0,random-forest:1,support-vector-machine:0');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Game of Thrones Season 8 Episode 3 – ''The Long Night''',8d5bdd50-7060-11e9-8eec-bb51378f83a7,'This episode showed the real threat to GOT, the writers.','2019-05-10 00:01:51',1,'pos','logistic-regression:1,random-forest:1,support-vector-machine:1');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Game of Thrones Season 8 Episode 3 – ''The Long Night''',8d5bdd50-7060-11e9-8eec-bb51378f83a7,'Its getting worse as you rewatch the episode. Fitstly It was Just ok i expected something more but I let It go. Then I have wacthed It again a few days later and I have realized It was very stupid.','2019-05-10 00:02:51',0,'neg','logistic-regression:0,random-forest:0,support-vector-machine:0');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Game of Thrones Season 8 Episode 3 – ''The Long Night''',8d5bdd50-7060-11e9-8eec-bb51378f83a7,'OK, firstly let it be known that I love game of throne. I have recently re-watched the entire series again in preparation for season 8. This episode for me was a complete and utter let down. Visually very boring with the same type of repetitive shot over and over again.  This was a long battle, yes. but long for the sake of being long does not equal good. It was bad. Very bad. The pseudo deaths were also extremely annoying, as soon as you think a major player was gone, they turn back up again to once again defeat the terrible odds they face. The plot armor is strong with this one. The CGI was sub-par and I think that is why they went with the muddy dark tone of the episode. This is not the Game of thrones I fell in love with all those years ago. This is just another mediocre, bloated contrived mess like all the other average shows out there. Come on GOT you''re better than this. You could of at least shown the scene where everyone was handed out teleportation juice and plot armor.  There were a few moments spread throughout the episode I thought were cool. But that''s it, a few moments. The episode as a whole was just so disappointing. I don''t understand all these 10/10 reviews, clearly blind fan boy/girl sheep who are in denial?  4/10 - please don''t ruin this series by jumping the shark with these last 3 episodes.','2019-05-10 00:03:57',0,'neg','logistic-regression:0,random-forest:0,support-vector-machine:0');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Game of Thrones Season 8 Episode 3 – ''The Long Night''',8d5bdd50-7060-11e9-8eec-bb51378f83a7,'This episode ruined the show so i gave 1/10. I hope, I just hope that there will be some twist in the next 3 episodes but I dont believe in that. Absolutely the worst possible ending to white walkers and the Night King. Also, the episode was too dark, I havent seen that "most epic battle in the history of tv" in this episode. We didnt see the clash of Jon Snow and Night King, we didnt hear what was Night Kings goal, why did he attack people, why did he wait for this long, we didnt see the real battle of Viserion and two living dragons... 7 seasons of building the great story ruined in 2 minutes, really I cant hide my dissapointment... Oh, I almost forgot, nothing happened between Bran and Night King.','2019-05-10 07:15:19',0,'neg','logistic-regression:0,random-forest:0,support-vector-machine:0');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Game of Thrones Season 8 Episode 3 – ''The Long Night''',8d5bdd50-7060-11e9-8eec-bb51378f83a7,'I have 10 IQ and I find this episode awesome. Thank you.','2019-05-10 07:20:26',1,'pos','logistic-regression:1,random-forest:1,support-vector-machine:1');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Game of Thrones Season 8 Episode 3 – ''The Long Night''',8d5bdd50-7060-11e9-8eec-bb51378f83a7,'What an anticlimactic peak of an episode. Beautiful, but the writing... Absolutely no words, this show is now dead to me. Who''s rating this 10/10? At best this episode is worth a 4/10, AT BEST. ALL 4 stars JUST for the visuals, the writing deserves absolutely ZERO stars. Now that I think of it, make it 3/10, the writing is at a -1.','2019-05-10 07:22:14',1,'pos','logistic-regression:1,random-forest:1,support-vector-machine:1');

/* Tomb Raider 2018 */
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Tomb Raider',729f1890-7422-11e9-9ae5-9116fc548b6b,'Amazing start, good action, deep enough character development. After 30 minutes the production becomes completely predictable and starts following the cliche guidelines of an low budget movie designed to "kill time" for the average house wife.  Despite Alicia Vikander''s great performance going further the story becomes extremely boring, I assume sensed by the director, who decided to flood all milestones in it with too much drama and extremely irritating 80s-cheap-movie dramatic music.  On top of everything else, the film deviates from the game story completely.  Well, it is not a total wreck but if you have played the game and enjoyed it even a little, you do not need to suffer this glorified hiccup of a production.','2019-05-11 21:33:14',0,'neg','logistic-regression:0,random-forest:1,support-vector-machine:0');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Tomb Raider',729f1890-7422-11e9-9ae5-9116fc548b6b,'Not sure what everyone was expecting or why even compare this to the first film or even the game. This was more of a prequel to the first film and it was really well done! Alicia Vikander was great in her role. A well deserved 8/10 from me.','2019-05-11 21:34:43',1,'pos','logistic-regression:1,random-forest:1,support-vector-machine:1');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Tomb Raider',729f1890-7422-11e9-9ae5-9116fc548b6b,'Just watched it on streaming and compared with so many other action movies it definitely holds own.  I''ve played the tomb raider games for years and I don''t see any problem with the way this movie portrayed a young Lara Croft.  I''ve seen reviews on here saying poor CGI, poor fighting etc.. What were those people watching? The oceans have been rendered probably with more skill than I have seen in any movie.  The tomb sequence wasn''t exceedingly drawn out as in the games so for me that was a bonus.  I liked it, but then what do I know. My favourite movie is Joe vs the Volcano and I also loved John Carter....','2019-05-11 21:35:58',0,'neg','logistic-regression:1,random-forest:0,support-vector-machine:0');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Tomb Raider',729f1890-7422-11e9-9ae5-9116fc548b6b,'I watched the movie last night and really liked it. I''ve never played any of the tomb raider games so I just look at it as any other action movie and think this was a very okay movie. I watched a long time ago Angelina Jolie as Tomb Raider and never liked it because they just go overboard with the whole female big ass and big boobs.  This movie shows a whole other side of Lara Croft and I personally like it much better. It''s not set up like your typical one man hero movie and I hope we see more of her in future Tomb Raider movies!','2019-05-11 21:36:48',1,'pos','logistic-regression:1,random-forest:1,support-vector-machine:0');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Tomb Raider',729f1890-7422-11e9-9ae5-9116fc548b6b,'Best game I played and worst movie I watched. queen''s army, her ability just been cut in the movies. the story is totally empty.','2019-05-11 21:38:34',0,'neg','logistic-regression:0,random-forest:1,support-vector-machine:0');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Tomb Raider',729f1890-7422-11e9-9ae5-9116fc548b6b,' was really surprised watching this film, knowing that some critics and viewers had given it really harsh reviews. No the film is not perfect but neither were the Angelina Jolie''s films at all. They were quite cheesy actually although I enjoyed them enough because I like Jolie. This film''s storyline is at least a bit more realistic and has coherence. And Alicia Vikander makes an excellent Lara Croft. I know people complained that there was not enough "tomb raiding" but this is an origin story, so I''m not sure what they were expecting. I think there are a lot of fanboys out there giving this film a harsh review because either Lara isn''t sexy enough or they dont believe a "regular" girl could be tough enough to kick some ass. The film is great and I really hope they make another one.','2019-05-11 21:39:41',1,'pos','logistic-regression:1,random-forest:1,support-vector-machine:1');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Tomb Raider',729f1890-7422-11e9-9ae5-9116fc548b6b,'Given the poor ratings I had low expectations for this movie but it was surprisingly very entertaining. I wouldn''t compare it to the Original movies with Angelina Jolie as it was just a different movie experience. I am still a fan of Angelina Jolie as Lara Croft but the Alicia Vikander version works too.','2019-05-11 21:40:20',0,'neg','logistic-regression:1,random-forest:0,support-vector-machine:0');
INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
VALUES(now(),'Tomb Raider',729f1890-7422-11e9-9ae5-9116fc548b6b,'I don''t get the 1-star reviews. This is definitely not the worst movie of the century. I can''t say it has a great storyline, but at least it has some storyline, unlike most Hollywood action movies these days. The things I liked: 1. The special effects. They looked real and Lara didn''t look like some plastic figure jumping and tumbling around and whatnot. She looked like Alicia Vikander, an actual person that has stuff happen to her. Apparently Alicia did some stunts herself so maybe they didn''t have to use so much CGI. And she looks very athletic so that adds to the overal feel of Lara being a spunky little adventuress. 2. Alicia as Lara. There''s been a lot of talk about her practically nonexistent breasts, and ''of course'' the real Lara is quite busty, but I find this talk ridiculous. Alicia looks stunning and sporty, and she oozes Lara Croft from every pore. Her acting was great too, it was very intense and realistic - the emotions and the sounds she made during the fight made it seem like it was all happening for real. You don''t usually see it in movies.','2019-05-11 21:41:40',1,'pos','logistic-regression:1,random-forest:0,support-vector-machine:1');


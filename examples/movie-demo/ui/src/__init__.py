#! /usr/bin/python3
# -*- coding:utf-8 -*-

# ******************************************************************************
#
# Sentiment Analysis UI
#
# Author: Fabrice LEBEL, 2019
#
# ******************************************************************************

import os
import io
import base64
import matplotlib
from matplotlib import pyplot as plt
from flask import Flask, render_template, request, url_for, redirect
import json
import datetime
from kafka import KafkaProducer
from cassandra.cluster import Cluster
import pandas as pd

matplotlib.use('Agg')

app = Flask(__name__)

# Apache Kafka broker connection parameters
kafka_broker_ip = "{}:{}".format(os.getenv("FLC_KAFKA_IP", default="127.0.0.1"),
                                 os.getenv("FLC_KAFKA_PORT", default="9092"))
print("INFO: Apache Kafka address: {}".format(kafka_broker_ip))

# Apache Cassandra cluster parameters
cassandra_cluster_ip = os.getenv("FLC_CASSANDRA_IP", default="127.0.0.1")
cassandra_cluster_port = os.getenv("FLC_CASSANDRA_PORT", default="9042")
print("INFO: Apache Cassandra address: {}:{}".format(cassandra_cluster_ip, cassandra_cluster_port))
cassandra_keyspace = 'movie_comments'

# Local webserver IP address and port
webserver_ip = os.getenv("FLC_WEBSERVER_IP", default="0.0.0.0")
webserver_port = os.getenv("FLC_WEBSERVER_PORT", default="8080")
print("INFO: Webserver listen address: {}:{}".format(webserver_ip, webserver_port))

# Connection to the Apache Cassandra cluster
cassandra_cluster = None
cassandra_session = None
try:
    cassandra_cluster = Cluster([cassandra_cluster_ip], port=cassandra_cluster_port)
    try:
        cassandra_session = cassandra_cluster.connect('sentiment_analysis')
        print("INFO: Connected to Apache Cassandra cluster")
    except Exception as keyspace_err:
        print("Cassandra keyspace error: {}".format(keyspace_err))
except Exception as cassandra_err:
    print("Cassandra cluster connection error: {}".format(cassandra_err))


def cql_escape(s):
    return s.replace("'", "''")


def remove_new_lines(s):
    return s.replace('\r', '').replace('\n', ' ')


def create_json(movie_name, movie_id, comment, timestamp):
    """
    Creates a JSON char string for a movie comment.

    :param movie_name: Movie name
    :param movie_id: Movie ID
    :param comment: User comment
    :param timestamp: Timestamp
    :return: JSON char string
    """
    tmp = {'movie_name': movie_name, 'movie_id': movie_id, 'comment': remove_new_lines(comment), 'timestamp': timestamp}
    return json.dumps(tmp).encode('utf-8')


def get_last_comments_db(movie_name, movie_id, num_rows=10):
    query = """
        SELECT * FROM  sentiment_analysis.kafka_comments WHERE movie_name='{}' AND movie_id={} LIMIT {};
        """.format(cql_escape(movie_name), movie_id, num_rows).strip()
    print("get_last_comments_db(): " + query)
    rows = cassandra_session.execute(query)
    results = []
    for row in rows:
        results.append({'movie_comment': row.movie_comment, 'datetime': row.datetime})
    print('get_last_comments_db(): ' + str(results))
    return results


def get_total_number_comments_db(movie_name, movie_id):
    query = """
         SELECT count(*) FROM  sentiment_analysis.kafka_comments WHERE movie_name='{}' AND movie_id={};
         """.format(cql_escape(movie_name), movie_id).strip()
    print("get_total_number_comments_db(): {}".format(query))
    rows = cassandra_session.execute(query)
    result = 0
    if rows:
        result = rows[0][0]
    return result


def get_last_comments_to_df(movie_name, movie_id, num_rows=10):
    query = """
        SELECT * FROM  sentiment_analysis.kafka_comments WHERE movie_name='{}' AND movie_id={} LIMIT {};
        """.format(cql_escape(movie_name), movie_id, num_rows).strip()
    print("get_last_comment_to_df(): {}".format(query))
    rows = cassandra_session.execute(query)

    # Create Pandas dataframe from CQL rows
    pd.set_option('display.max_colwidth', -1)
    df = pd.DataFrame(rows)
    return df


def create_frequency_table_figure(movie_name, movie_id):
    query = """
        SELECT COUNT(sentiment_value) FROM sentiment_analysis.kafka_comments WHERE movie_name='{}' AND movie_id={} AND sentiment_value=0;
        """.format(cql_escape(movie_name), movie_id).strip()
    print('create_frequency_table_figure() #1: {}'.format(query))
    rows = cassandra_session.execute(query)
    negative_value = 0
    if rows:
        negative_value = rows[0][0]
    print("create_frequency_table_figure() #1: # negative values: {}".format(negative_value))

    query = """
        SELECT COUNT(sentiment_value) FROM sentiment_analysis.kafka_comments WHERE movie_name='{}' AND movie_id={} AND sentiment_value=1;
        """.format(cql_escape(movie_name), movie_id).strip()
    print('create_frequency_table_figure() #2: {}'.format(query))
    rows = cassandra_session.execute(query)
    positive_value = 0
    if rows:
        positive_value = rows[0][0]
    print("create_frequency_table_figure() #2: # positive values: {}".format(positive_value))

    names = ['Negative', 'Positive']
    values = [negative_value, positive_value]
    colors = ['salmon', 'green']
    fig, axs = plt.subplots(1, 2)
    fig.set_figwidth(10)
    plt.show(block=False)
    # Histogram of frequencies
    axs[0].bar(names, values, color=colors)
    axs[0].set_ylabel('Frequency')
    axs[0].set_title('Frequency of Negative/Positive Comments')
    # Percentages graph
    axs[1].pie(values, labels=names, autopct='%1.1f%%', colors=colors, shadow=True, startangle=90)
    axs[1].set_title('Percentage of Negative/Positive Comments')

    img = io.BytesIO()
    img.seek(0)
    plt.savefig(img, format='png')
    graph_url = base64.b64encode(img.getvalue()).decode()
    img.close()
    plt.close()
    return 'data:image/png;base64,{}'.format(graph_url)


def create_top_words_figure(movie_name, movie_id, num_rows=10):
    query = """
        SELECT word, word_frequency, datetime FROM sentiment_analysis.movie_wordcount WHERE movie_name='{}' AND movie_id={} LIMIT {};
        """.format(cql_escape(movie_name), movie_id, num_rows).strip()
    print('create_top_words_figure(): {}'.format(query))
    rows = cassandra_session.execute(query)
    words = []
    frequencies = []
    last_datetime = '?'
    for row in rows:
        words.append(row.word)
        frequencies.append(row.word_frequency)
        last_datetime = row.datetime
    fig, ax = plt.subplots(1, 1)
    fig.set_figwidth(10)
    plt.show(block=False)
    plt.yticks(rotation=40)

    # Histogram of frequencies
    ax.barh(words, frequencies, color='blue')
    ax.set_ylabel('Words')
    ax.set_title('Top {} Words\n(updated at {})'.format(num_rows, last_datetime))
    img = io.BytesIO()
    img.seek(0)
    plt.savefig(img, format='png')
    graph_url = base64.b64encode(img.getvalue()).decode()
    img.close()
    plt.close()
    return 'data:image/png;base64,{}'.format(graph_url)


def insert_movie_db(movie_name, description, year):
    query = """ INSERT INTO sentiment_analysis.movie  (movie_id, movie_name, description, year) 
    VALUES (now(), '{}', '{}', {}) IF NOT EXISTS;""".format(cql_escape(movie_name), cql_escape(description),
                                                            year).strip()
    print('insert_movie(): ' + query)
    cassandra_session.execute(query)


def get_movie_db(movie_name, movie_id):
    query = """ SELECT * FROM sentiment_analysis.movie WHERE movie_name='{}' AND movie_id={}; """.format(
        cql_escape(movie_name), movie_id).strip()
    print('get_movie(): ' + query)
    row = cassandra_session.execute(query).__getitem__(0)
    print('get_movie(): Row[0] value: ' + str(row))
    result = {'movie_name': row.movie_name, 'movie_id': row.movie_id, 'description': row.description, 'year': row.year}
    print('get_movie(): dictionary value: ' + str(result))
    return result


def get_movies_db(pattern):
    query = """
         SELECT movie_name, movie_id, description, year FROM sentiment_analysis.movie; """.strip()
    print('get_movies_db(): {}'.format(query))
    rows = cassandra_session.execute(query)
    results = []
    for row in rows:
        # Filter with 'pattern'
        if row.movie_name.lower().find(pattern.lower()) > -1:
            results.append([row.movie_name, row.movie_id, row.description, row.year])
    print('get_movies_db(): results: {}'.format(str(results)))
    return results


def delete_movie_db(movie_name, movie_id):
    # Delete all word counts related to the movie
    query = """ DELETE FROM sentiment_analysis.movie_wordcount WHERE movie_name='{}' AND movie_id={}; """.format(
        cql_escape(movie_name), movie_id).strip()
    print('delete_movie_db(): {}'.format(query))
    cassandra_session.execute(query)
    # Remove all comments related to the movie
    query = """ DELETE FROM sentiment_analysis.kafka_comments WHERE movie_name='{}' AND movie_id={}; """.format(
        cql_escape(movie_name), movie_id).strip()
    print('delete_movie_db(): {}'.format(query))
    cassandra_session.execute(query)
    # Remove the movie
    query = """ DELETE FROM sentiment_analysis.movie WHERE movie_name='{}' AND movie_id={}; """.format(
        cql_escape(movie_name), movie_id).strip()
    print('delete_movie_db(): {}'.format(query))
    cassandra_session.execute(query)
    return


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        if request.form['btn'] == 'Add movie to database':
            return redirect(url_for('add_movie'))
        elif request.form['btn'] == 'Search...':
            movie_name = request.form.get('movie_to_search_for')
            movies = get_movies_db(movie_name)
            return render_template('index.html', movies=movies)
    return render_template('index.html')


@app.route('/add_movie/', methods=['GET', 'POST'])
def add_movie():
    if request.method == 'POST':
        if request.form['btn'] == '< Back':
            return redirect(url_for('index'))
        elif request.form['btn'] == 'Save':
            movie_name = request.form.get('movie_name')
            if movie_name.strip() is not '':
                movie_description = request.form.get('movie_description')
                year = int(request.form.get('year'))
                insert_movie_db(movie_name, movie_description, year)
            return render_template('add_movie.html')
    return render_template('add_movie.html')


@app.route('/movie/<movie_name>/<movie_id>', methods=['GET', 'POST'])
def movie(movie_name, movie_id):
    movie_data = get_movie_db(movie_name, movie_id)
    movie_comments = get_last_comments_db(movie_name, movie_id)
    if request.method == 'POST':
        if request.form['btn'] == '< Back':
            return redirect(url_for('index'))
        elif request.form['btn'] == 'Refresh':
            return render_template('movie.html', movie_data=movie_data, movie_comments=movie_comments)
        elif request.form['btn'] == 'Sentiment analysis':
            return redirect(url_for('movie_statistics', movie_name=movie_name, movie_id=movie_id))
        elif request.form['btn'] == 'Delete':
            return redirect(url_for('delete_movie', movie_name=movie_name, movie_id=movie_id))
        elif request.form['btn'] == 'Submit comment':
            comment = request.form.get('comment')
            if comment.strip() is not '':
                # Create timestamp
                timestamp = '{:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())
                # Create JSON message
                json_str = create_json(movie_name, movie_id, comment, timestamp)
                print('movie(): {}'.format(json_str))
                try:
                    # Send JSON message to Kafka broker with a Kafka Producer
                    producer = KafkaProducer(bootstrap_servers=[kafka_broker_ip])
                    try:
                        producer.send('movie_comments', key=None, value=json_str)
                        return render_template('movie.html', movie_data=movie_data, comment_value='',
                                               movie_comments=movie_comments)
                    except Exception as send_error:
                        print("Producer error: {}".format(send_error))
                        return render_template('movie.html', movie_data=movie_data, comment_value=comment,
                                               movie_comments=movie_comments)
                    finally:
                        producer.close()
                except Exception as producer_error:
                    print("Producer error: {}".format(producer_error))
                    return render_template('movie.html', movie_data=movie_data, comment_value=comment,
                                           movie_comments=movie_comments)
                finally:
                    pass
            else:
                return render_template('movie.html', movie_data=movie_data, comment_value='',
                                       movie_comments=movie_comments)
    elif request.method == 'GET':
        return render_template('movie.html', movie_data=movie_data, movie_comments=movie_comments)


@app.route('/statistics/<movie_name>/<movie_id>', methods=['GET', 'POST'])
def movie_statistics(movie_name, movie_id):
    df = get_last_comments_to_df(movie_name, movie_id)
    if request.method == 'POST':
        if request.form['btn'] == 'Home':
            return redirect(url_for('index'))
        if request.form['btn'] == '< Back':
            return redirect(url_for('movie', movie_name=movie_name, movie_id=movie_id))
    return render_template('movie_statistics.html', movie_name=movie_name, movie_id=movie_id,
                           total_comments=get_total_number_comments_db(movie_name, movie_id),
                           frequency_table_fig=create_frequency_table_figure(movie_name, movie_id),
                           top_words_fig=create_top_words_figure(movie_name, movie_id, 20),
                           pandas_table_content=df.to_html(classes='table table-striped table-hover',
                                                           header="true",
                                                           table_id="table"))


@app.route('/delete_movie/<movie_name>/<movie_id>', methods=['GET', 'POST'])
def delete_movie(movie_name, movie_id):
    if request.method == 'POST':
        if request.form['btn'] == '< Back':
            return redirect(url_for('movie', movie_name=movie_name, movie_id=movie_id))
        elif request.form['btn'] == 'Delete':
            delete_movie_db(movie_name, movie_id)
            return redirect(url_for('index'))
    return render_template('delete_movie.html', movie_name=movie_name, movie_id=movie_id)


if __name__ == '__main__':
    app.run(debug=True, host=webserver_ip, port=webserver_port)

#!/bin/sh
# ******************************************************************************
#
# Cleanup development environment.
#
# (C) 2019, Fabrice LEBEL, fabrice.lebel.pro@outlook.com
#
# ******************************************************************************

# Remove Python virtual environment
rm -rf ./venv


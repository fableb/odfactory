#!/bin/sh
# ******************************************************************************
#
# Script to create a Docker container for the web server.
#
# (C) 2019, Fabrice LEBEL, fabrice.lebel.pro@outlook.com
#
# ******************************************************************************
USER_NAME="flc"
IMAGE_NAME="webserver"
TAG_NAME="latest"

echo "Start creating Docker container..."
echo

if [ -z $1]; then
    echo "No user name provided. Using '${USER_NAME}'."
else
    USER_NAME=$1
fi

if [ -z $2 ]; then
    echo "No image name provided. Using '${IMAGE_NAME}'."
else
    IMAGE_NAME=$2
fi

if [ -z $3 ]; then
    echo "No tag name provided. Using '${TAG_NAME}'."
else
    TAG_NAME=$3
fi

echo "Docker Tag will be: '${USER_NAME}/${IMAGE_NAME}:${TAG_NAME}'"
echo

read -p "Proceed? [y|n]: " response
if [ ${response} != "y" ]; then
    echo "Abort."
    exit 1
fi
echo

# Build Docker container
docker build -f ./docker-src/Dockerfile -t ${USER_NAME}/${IMAGE_NAME}:${TAG_NAME} .

echo
echo "Creating Docker container done."

import sbt.Keys.{dependencyOverrides, version}
import sbtassembly.AssemblyPlugin.autoImport.{ShadeRule, assemblyJarName, assemblyOption}

name := "servers"

version := "0.1"

scalaVersion := "2.11.8"

val sparkVersion = "2.1.0"

scalacOptions ++= Seq("-deprecation")

resolvers += "Spark Packages Repo" at "https://dl.bintray.com/spark-packages/maven"

libraryDependencies ++= Seq(
  "com.typesafe" % "config" % "1.3.3",
  "io.circe" %% "circe-core" % "0.10.0",
  "io.circe" %% "circe-generic" % "0.10.0",
  "io.circe" %% "circe-parser" % "0.10.0",
  "org.apache.kafka" %% "kafka" % "2.1.0" withSources(),
  "org.apache.kafka" % "kafka-clients" % "2.1.0" withSources(),
  "com.datastax.spark" %% "spark-cassandra-connector" % "2.4.1",
  "commons-configuration" % "commons-configuration" % "1.9",
  "org.apache.spark" %% "spark-core" % sparkVersion % "provided" withSources(),
  "org.apache.spark" %% "spark-sql" % sparkVersion % "provided" withSources(),
  "org.apache.spark" %% "spark-mllib" % sparkVersion % "provided" withSources()
)

unmanagedBase := baseDirectory.value / "lib"

/*lazy val commonSettings = Seq(
  libraryDependencies ++= Seq(
    "com.typesafe" % "config" % "1.3.3"
  )
)*/

lazy val `servers` = project
  .in(file("."))
  //.settings(commonSettings: _*)
  .settings(
    version := "0.1.0",
    name := "server",
    assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false),
    assemblyJarName in assembly := s"${name.value}_${scalaBinaryVersion.value}-${sparkVersion}_${version.value}.jar"
  )
  .enablePlugins(AssemblyPlugin)
  .aggregate(`comment-classifier`)

lazy val `comment-classifier` = (project in file("CommentClassifierServer"))
  //.settings(commonSettings: _*)
  .settings(
    version := "0.1.0",
    name := "comment-classifier",
    mainClass in assembly := Some("CommentClassifierServer"),
    assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false),
    assemblyJarName in assembly := s"${name.value}_${scalaBinaryVersion.value}-${sparkVersion}_${version.value}.jar"
  )

lazy val `wordcount` = (project in file("WordCountServer"))
  //.settings(commonSettings: _*)
  .settings(
    version := "0.1.0",
    name := "wordcount",
    mainClass in assembly := Some("WordCountServer"),
    assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false),
    assemblyJarName in assembly := s"${name.value}_${scalaBinaryVersion.value}-${sparkVersion}_${version.value}.jar"
  )
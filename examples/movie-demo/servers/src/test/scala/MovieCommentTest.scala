import org.scalatest.FunSuite

/**
  * @author Fabrice LEBEL, fabrice.lebel.pro@outlook.com
  */
class MovieCommentTest extends FunSuite {
  val movieComment1: MovieComment = MovieComment("one", "123", "this is a comment", "2019-08-15")

  test("testMovieName") {
    assert(movieComment1.movieName ==="one")
  }

  test("testMovieID") {
    assert(movieComment1.movieID === "123")
  }

  test("testComment") {
    assert(movieComment1.comment === "this is a comment")
  }

  test("testTimestamp") {
    assert(movieComment1.timestamp === "2019-08-15")
  }

  test("testCopy") {
    val movieComment2 = movieComment1.copy()
    assert(movieComment2 === movieComment1)
  }
}

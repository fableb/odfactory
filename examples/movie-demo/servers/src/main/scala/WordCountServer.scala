/**
  * @author Fabrice LEBEL, fabrice.lebel.pro@outlook.com
  */
import java.io.{File, InputStream}
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import com.typesafe.config.ConfigFactory
import com.datastax.driver.core.Session
import com.datastax.spark.connector.cql.CassandraConnector
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{explode, lit}
import fllemmatizer.FLLemmatizer

import scala.collection.JavaConversions._

object WordCountServer {
  def getLinesToArray(path: String): Array[String] = {
    val stream: InputStream = getClass.getResourceAsStream(path)
    val lines = scala.io.Source.fromInputStream(stream).getLines.toArray
    lines
  }

  def createTimestamp(): String = {
    // Time stamp with the form "yyyy-mm-dd hh:mm;ss" (i.e. "2019-05-07 02:56:48")
    LocalDateTime.now.format(DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss"))
  }

  def main(args: Array[String]): Unit = {
    // Load configuration file
    println("INFO: Load configuration file '" + args(0) + "'")
    val config = ConfigFactory.parseFile(new File(args(0)))

    // Connect to Apache Spark
    val clusterIPAddress: String = config.getString("appConf.cassandra.clusterIPAddress")
    val clusterPort: Int = config.getInt("appConf.cassandra.clusterPort")

    val sparkSession = SparkSession
      .builder
      .appName("Sentiment Analysis App - WordCount")
      .master(config.getString("appConf.spark.master"))
      .config("spark.cassandra.connection.host", clusterIPAddress)
      .config("spark.cassandra.connection.port", clusterPort)
      .getOrCreate()

    sparkSession.sparkContext.setLogLevel("ERROR")
    println("INFO: Connected to Apache Spark cluster (version " + sparkSession.sparkContext.version + ")")

    // Connect to Apache Cassandra cluster
    try {
      val cassandraConnector: CassandraConnector = CassandraConnector.apply(sparkSession.sparkContext.getConf)
      val cassandraSession: Session = cassandraConnector.openSession()

      val rs = cassandraSession.execute("SELECT release_version FROM system.local") // Version du framework
      val row = rs.one

      val databaseName: String = config.getString("appConf.cassandra.databaseName")
      cassandraSession.execute("USE " + databaseName)
      println("INFO: Connected to Apache Cassandra cluster (version "
        + row.getString("release_version") + ") at address '"
        + clusterIPAddress + ":" + clusterPort + "'")
      println("INFO: Using keyspace '" + databaseName + "'...")

      val wordcountBatchIntervalMs = config.getInt("appConf.cassandra.wordcountBatchIntervalMs")
      while (true) {
        // Load 'movie' table
        val movieTableName: String = config.getString("appConf.cassandra.movieTableName")
        val movieDF = sparkSession.read
          .format("org.apache.spark.sql.cassandra")
          .options(Map("keyspace" -> databaseName, "table" -> movieTableName))
          .load()
        //movieDF.printSchema()
        movieDF.show()

        // Load 'kafka_comments' table
        val commentTableName: String = config.getString("appConf.cassandra.commentTableName")
        val commentDF = sparkSession.read
          .format("org.apache.spark.sql.cassandra")
          .options(Map("keyspace" -> databaseName, "table" -> commentTableName))
          .load()
        //commentDF.printSchema()
        commentDF.show()

        // Create views for SQL requests
        movieDF.createOrReplaceTempView("movie")
        commentDF.createOrReplaceTempView("comment")

        // Join 'movie' and 'kafka_comments' tables
        val joinedMovieComments = sparkSession.sql(
          s"""
             |SELECT l.movie_name, l.movie_id, r.movie_comment
             |FROM movie AS l LEFT OUTER JOIN comment AS r
             |WHERE l.movie_id = r.movie_id
         """.stripMargin)
        joinedMovieComments.show()

        // *********
        // NLP steps
        // *********
        // For each movie
        //   1. aggregate comments (create a corpus)
        //   2. tokenize comments (NLP part)
        //   3. lemmatize words (NLP part)
        //   4. remove stop-words (NLP part)
        //   5. compute each word frequency
        //   6. save to Apache Cassandra

        // Aggregate comments by transforming Dataset/Dataframe into RDD[(K,V)]
        import sparkSession.implicits._ // for type conversion purpose

        val movieCommentsRDD: RDD[(String, Iterable[String])] = joinedMovieComments.select("movie_id", "movie_comment")
          .map(x => (x.getAs[String]("movie_id"), x.getAs[String]("movie_comment")))
          .rdd
          .groupByKey()
        movieCommentsRDD.foreach(println)

        // Merge comments of a movie into a single string
        val mergedCommentsRDD: RDD[(String, String)] = movieCommentsRDD.mapValues(_.mkString(" "))
        mergedCommentsRDD.foreach(println)

        // NLP part
        val stopwords = getLinesToArray("/stopwords.txt")

        val wordcountRDD = mergedCommentsRDD
          .mapValues(x => {
            val lemmatizer = new FLLemmatizer("en")
            lemmatizer
              .lemmatize(x, true) // lemmatization and POS tagging
              .filter(x => x(1) != "PUNC") // remove punctuation
              .map(x => x(2)) // get lemmas
              .filter(x => x.matches("[A-Za-z]+") && x.length > 2) // remove non alphabetical characters
              .filter(x => !stopwords.contains(x)) // remove stop-words
              .map(x => (x, 1)) // word count map
              .groupBy(x => x._1)
              .mapValues(_.size) // word count reduce (word frequency)
          })
        wordcountRDD.foreach(println)

        // Transform a Map[(String, Int)] = Map[('word', 'frequency')] into columns 'word' and 'frequency'
        val wordcountDF = wordcountRDD
          .toDF("movie_id", "wordcount_map") // create a data frame with named columns
          .select($"movie_id", explode($"wordcount_map")).toDF("movie_id", "word", "word_frequency") // explode Map[('word', 'frequency')] into 2 named columns: 'word' and 'frequency'
          .join(movieDF.select("movie_name", "movie_id"), "movie_id") // add 'movie_name' column
          .withColumn("datetime", lit(createTimestamp())) // add 'datetime' column
        //wordcountDF.printSchema()
        wordcountDF.show()

        // Save to 'word_count' table
        val wordcountTableName: String = config.getString("appConf.cassandra.wordcountTableName")
        wordcountDF.write
          .mode("overwrite")
          .format("org.apache.spark.sql.cassandra")
          .options(Map("table" -> wordcountTableName, "keyspace" -> databaseName, "confirm.truncate" -> "true"))
          .save()

        Thread.sleep(wordcountBatchIntervalMs)
      }
    }
    finally {
    }
    // Close Apache Spark session
    sparkSession.close()
  }
}

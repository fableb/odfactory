/**
  * @author Fabrice LEBEL, fabrice.lebel.pro@outlook.com
  */
case class MovieComment(movieName: String, movieID: String, comment: String, timestamp: String) {
  def this() = this("", "", "", "")

  def copy(): MovieComment = MovieComment(this.movieName, this.movieID, this.comment, this.timestamp)
}
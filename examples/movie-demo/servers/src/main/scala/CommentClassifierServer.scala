/**
  * Uses several algorithmes to perform sentiment analysis.
  *
  * @author Fabrice LEBEL, fabrice.lebel.pro@outlook.com
  */
import java.io.File
import java.nio.file.Paths
import java.time.Duration
import java.util.{Collections, Properties}

import com.typesafe.config.ConfigFactory
import com.datastax.driver.core.{Cluster, Session}
import io.circe._
import io.circe.parser._
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.classification.{LogisticRegressionModel, RandomForestClassificationModel}
import org.apache.spark.mllib.classification.SVMModel
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, udf}

import scala.collection.JavaConverters._

object CommentClassifierServer {
  def majorityVote(x: Map[String, Int]): Int = {
    if (x.values.sum >= Math.ceil(x.size / 2.0)) 1 // positif
    else 0 // négatif
  }

  def saveToCassandra(cassandraSession: Session, comment: MovieComment,
                      sentiment_value: Int, algorithms: Map[String, Int]): Unit = {
    var sentiment_label: String = ""
    if (sentiment_value == 1) sentiment_label = "pos"
    else if (sentiment_value == 0) sentiment_label = "neg"
    else sentiment_label = "NA"

    val algorithmsStr = algorithms.map(x => x._1.toString + ":" + x._2.toString).mkString(",")

    val movieNameEscaped = comment.movieName.replaceAll("'", "''")
    val commentEscaped = comment.comment.replaceAll("'", "''")
    val query: String =
      s"""INSERT INTO kafka_comments(comment_id,movie_name,movie_id,movie_comment,datetime,sentiment_value,sentiment_label,algorithms)
         |VALUES(now(),'$movieNameEscaped',${comment.movieID},'$commentEscaped','${comment.timestamp}',$sentiment_value,'$sentiment_label','$algorithmsStr')""".stripMargin
    try {
      println("INFO: Executing query: " + query)
      cassandraSession.execute(query)
    } finally {

    }
  }

  def main(args: Array[String]): Unit = {
    // Get program current working directory
    val programDirectory = Paths.get(".").toRealPath()
    println("INFO: Program current working directory : " + programDirectory)

    // Load configuration file
    println("INFO: Load configuration file '" + args(0) + "'")
    val config = ConfigFactory.parseFile(new File(args(0)))

    // Décodeur json pour io.circe
    implicit val decodeUser: Decoder[MovieComment] = Decoder
      .forProduct4("movie_name", "movie_id", "comment", "timestamp")(MovieComment.apply)

    // ************************************************
    // Paramétrage et connexion au cluster Apache Spark
    // ************************************************
    val spark = SparkSession
      .builder
      .appName("Sentiment Analysis App - CommentClassifier")
      .master(config.getString("appConf.spark.master"))
      .getOrCreate()

    spark.sparkContext.setLogLevel("ERROR")
    println("INFO: Connected to Apache Spark cluster (version " + spark.sparkContext.version + ")")

    // Chargement du pipeline de création du TF-IDF et des modèles pré-entrainés
    val modelsPath = config.getString("appConf.modelsPath")
    val tfidfPipeline = Pipeline.load(modelsPath + "/Spark-TFIDF")
    val logisticRegressionModel = LogisticRegressionModel.load(modelsPath + "/Spark-LogisticRegressionModel")
    val randomForestModel = RandomForestClassificationModel.load(modelsPath + "/Spark-RandomForestModel")
    val supportVectorMachineModel = SVMModel.load(spark.sparkContext, modelsPath + "/Spark-SupportVectorMachineModel")

    // ****************************************************
    // Paramétrage et connexion au cluster Apache Cassandra
    // ****************************************************
    val databaseName: String = config.getString("appConf.cassandra.databaseName")
    //val commentTableName: String = config.getString("appConf.cassandra.commentTableName")
    val clusterIPAddress: String = config.getString("appConf.cassandra.clusterIPAddress")
    val clusterPort: Int = config.getInt("appConf.cassandra.clusterPort")
    var cassandraCluster: Cluster = null
    var cassandraSession: Session = null
    try {
      // Connexion au cluster Cassandra
      cassandraCluster = Cluster.builder.addContactPoint(clusterIPAddress).withPort(clusterPort).build
      cassandraSession = cassandraCluster.connect

      val rs = cassandraSession.execute("select release_version from system.local") // Version du framework
      val row = rs.one
      println("INFO: Connected to Apache Cassandra cluster (version "
        + row.getString("release_version") + ") at address '"
        + clusterIPAddress + ":" + clusterPort + "'")
      println("INFO: Using keyspace '" + databaseName + "'...")
      cassandraSession.execute("USE " + databaseName)
    } finally {
      //if (cassandraCluster != null) cassandraCluster.close()
    }

    // ************************************************
    // Paramétrage et connexion au cluster Apache Kafka
    // ************************************************
    val properties = new Properties()
    properties.put("bootstrap.servers", config.getString("appConf.kafka.kafkaBrokerHDP"))
    properties.put("group.id", config.getString("appConf.kafka.groupID"))
    properties.put("key.deserializer", classOf[StringDeserializer])
    properties.put("value.deserializer", classOf[StringDeserializer])

    val kafkaConsumer = new KafkaConsumer[String, String](properties)
    kafkaConsumer.subscribe(Collections.singleton(config.getString("appConf.kafka.topic")))
    println("INFO: Connected to Apache Kafka cluster at address '" + config.getString("appConf.kafka.kafkaBrokerHDP") + "'")

    // Suppression des caractères non alphabétiques
    val removeNonAlpha = udf((sentiment: String) => {
      sentiment.replaceAll("[^a-zA-Z\\s]", "")
    })

    try {
      while (true) {
        val records = kafkaConsumer.poll(Duration.ofSeconds(2))
        for (record <- records.asScala) {
          println("INFO: Fetched record: " + record)

          val decodedJSON = decode[MovieComment](record.value()) // Décodage du json en objet MovieComment
          var movieComment: MovieComment = null
          decodedJSON match {
            case Right(value) => movieComment = value.copy() // Récupération de l'objet MovieComment
          }
          println("INFO: " + movieComment)

          // Stockage du commentaire dans une datafame.
          //
          // !!! Remarque !!! : Nous devons avoir au moins 2 lignes dans
          // la dataframe sinon la méthode transform() renvoie toujours les
          // mêmes probabilités : 'bug' ???
          val unlabeled = spark.createDataFrame(Seq(
            (1L, "This is a dummy comment to correct a bug in Spark. Need at least 2 rows."),
            (2L, movieComment.comment))
          ).toDF("id", "comment")
            .withColumn("clean_comment", removeNonAlpha(col("comment")))

          // Création de la matrice TF-IDF du documents
          val unlabeledFitted = tfidfPipeline.fit(unlabeled).transform(unlabeled)

          // Prédiction du sentiment avec les algorithmes pré-entrainés
          val predictLogisticRegression = logisticRegressionModel.transform(unlabeledFitted)
          val predictionLogisticRegression = predictLogisticRegression
            .select("id", "clean_comment", "probability", "prediction")
            .collect()(1)
          println("INFO: Logistic Regression Prediction: " + predictionLogisticRegression)
          val sentimentValueLogisticRegression: Int = predictionLogisticRegression(3).toString.toDouble.toInt

          val predictRandomForest = randomForestModel.transform(unlabeledFitted)
          val predictionRandomForest = predictRandomForest
            .select("id", "clean_comment", "probability", "prediction")
            .collect()(1)
          println("INFO: Random Forest Prediction: " + predictionRandomForest)
          val sentimentValueRandomForest: Int = predictionRandomForest(3).toString.toDouble.toInt

          val unlabeledFittedRDD = unlabeledFitted.rdd.map(row =>
            org.apache.spark.mllib.linalg.Vectors.fromML(row.getAs[org.apache.spark.ml.linalg.SparseVector]("tfidf"))
          )
          val predictionSVM = supportVectorMachineModel.predict(unlabeledFittedRDD.collect()(1))
          val sentimentValueSVM = predictionSVM.toString.toDouble.toInt
          println("INFO: Support Vector Machine: " + predictionSVM)

          // Résulats des différents algorithmes
          val algorithms = Map("logistic-regression" -> sentimentValueLogisticRegression,
            "random-forest" -> sentimentValueRandomForest,
            "support-vector-machine" -> sentimentValueSVM)

          // Vote à la majorité pour un sentiment global
          val vote = majorityVote(algorithms)

          // Sauvegarde du commentaire et des résultats de l'analyse de
          // sentiments dans Apache Cassandra
          saveToCassandra(cassandraSession, movieComment, vote, algorithms)
        }
        Thread.sleep(1000)
      }
    } finally {
      if (kafkaConsumer != null) kafkaConsumer.close()
      if (cassandraCluster != null) cassandraCluster.close()
    }
  }
}

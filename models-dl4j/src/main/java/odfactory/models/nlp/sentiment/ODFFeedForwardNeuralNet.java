package odfactory.models.nlp.sentiment;

import com.beust.jcommander.Parameter;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function2;
import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.transform.TransformProcess;
import org.datavec.api.transform.condition.Condition;
import org.datavec.api.transform.condition.ConditionOp;
import org.datavec.api.transform.condition.column.IntegerColumnCondition;
import org.datavec.api.transform.filter.Filter;
import org.datavec.api.transform.quality.DataQualityAnalysis;
import org.datavec.api.transform.schema.Schema;
import org.datavec.api.writable.IntWritable;
import org.datavec.api.writable.Writable;
import org.datavec.spark.transform.AnalyzeSpark;
import org.datavec.spark.transform.SparkTransformExecutor;
import org.datavec.spark.transform.misc.StringToWritablesFunction;
import org.datavec.spark.transform.misc.WritablesToStringFunction;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.spark.api.RDDTrainingApproach;
import org.deeplearning4j.spark.api.TrainingMaster;
import org.deeplearning4j.spark.impl.multilayer.SparkDl4jMultiLayer;
import org.deeplearning4j.spark.parameterserver.training.SharedTrainingMaster;
import org.deeplearning4j.text.documentiterator.LabelAwareIterator;
import org.deeplearning4j.text.documentiterator.SimpleLabelAwareIterator;
import org.deeplearning4j.text.tokenization.tokenizer.Tokenizer;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.learning.config.Nadam;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.nd4j.parameterserver.distributed.conf.VoidConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.List;

/**
 * @author Fabrice LEBEL, fabrice.lebel.pro@outlook.com
 */
public class ODFFeedForwardNeuralNet {
    private String APP_NAME = "FeedForwardClassifier";
    private Config config = ConfigFactory.load();
    private static final Logger log = LoggerFactory.getLogger(ODFFeedForwardNeuralNet.class);

    @Parameter(names = "-useSparkLocal", description = "Use spark local (helper for testing/running without spark submit)", arity = 1)
    private boolean useSparkLocal = true;

    @Parameter(names = "-batchSizePerWorker", description = "Number of examples to fit each worker with")
    private int batchSizePerWorker = 16;

    @Parameter(names = "-numEpochs", description = "Number of epochs for training")
    private int numEpochs = 3;

    public static void main(String[] args) {
        new ODFFeedForwardNeuralNet().entryPoint(args);
    }

    private static class RemoveHeader implements Function2<Integer, Iterator<String>, Iterator<String>> {
        public Iterator<String> call(Integer ind, Iterator<String> iterator) throws Exception {
            if (ind == 0 && iterator.hasNext()) {
                iterator.next();
                return iterator;
            } else
                return iterator;
        }
    }

    private JavaRDD<List<Writable>> loadData(JavaSparkContext sc, String path) {
        Schema schema = new Schema.Builder()
                .addColumnString("id")
                .addColumnInteger("rating")
                .addColumnsString("sentiment", "raw_comment", "clean_comment")
                .build();

        Condition condition = new IntegerColumnCondition("rating", ConditionOp.GreaterThan, 5);
        TransformProcess tp = new TransformProcess.Builder(schema)
                .conditionalReplaceValueTransformWithDefault("rating", new IntWritable(1), new IntWritable(0), condition)
                .removeColumns("id", "sentiment", "raw_comment")
                .build();

        JavaRDD<String> stringData = sc.textFile(path);
        // !!! Skip header because CSVRecordReader is bugged and cannot do it (BUG) !!!
        JavaRDD<String> tmpStringData = stringData.mapPartitionsWithIndex(new RemoveHeader(), false);

        // Parse comma-delimited (CSV) file with CSVRecordReader:
        RecordReader rr = new CSVRecordReader(0, ',');
        JavaRDD<List<Writable>> parsedInputData = tmpStringData.map(new StringToWritablesFunction(rr));

        DataQualityAnalysis dataQualityAnalysis = AnalyzeSpark.analyzeQuality(schema, parsedInputData);
        System.out.println(dataQualityAnalysis);

        JavaRDD<List<Writable>> processedData = SparkTransformExecutor.execute(parsedInputData, tp);
        return processedData;
    }

    private List<String> getTokens(String s) {
        TokenizerFactory tokenizerFactory = new DefaultTokenizerFactory();
        tokenizerFactory.setTokenPreProcessor(new CommonPreprocessor());
        List<String> tokens = tokenizerFactory.create(s).getTokens();
        return tokens;
    }

    protected void entryPoint(String[] args) {
        SparkConf sparkConf = new SparkConf();
        if (useSparkLocal) {
            sparkConf.setMaster("local[*]");
        }
        sparkConf.setAppName(APP_NAME);
        JavaSparkContext sc = new JavaSparkContext(sparkConf);

        // Load train and test data sets
        System.out.println("---------- Train data set ----------");
        JavaRDD<List<Writable>> trainData = loadData(sc, "../data/train_dataset.csv");
        JavaRDD<String> trainDataAsString = trainData.map(new WritablesToStringFunction(","));
        List<String> trainDataAsStringCollected = trainDataAsString.take(5);
        for (String s : trainDataAsStringCollected) {
            System.out.println(s);
        }

        System.out.println("---------- Test data set ----------");
        JavaRDD<List<Writable>> testData = loadData(sc, "../data/test_dataset.csv");
        JavaRDD<String> testDataAsString = testData.map(new WritablesToStringFunction(","));
        List<String> testDataAsStringCollected = testDataAsString.take(5);
        for (String s : testDataAsStringCollected) {
            System.out.println(s);
        }

        // Create TF-IDF matrices for train and test data sets
        System.out.println("---------- Tokens ----------");
        List<String> tokens = getTokens("This is a long sentence to tokenize. DL4J is a little mess for the moment!");
        for (String t : tokens) {
            System.out.println(t);
        }
        // ??? Use Vocab Cache for TF-IDF ???


        // Create network configuration and perform training
        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(12345)
                .activation(Activation.LEAKYRELU)
                .weightInit(WeightInit.XAVIER)
                .updater(new Nadam())// To configure: .updater(Nesterovs.builder().momentum(0.9).build())
                .l2(1e-4)
                .list()
                .layer(new DenseLayer.Builder().nIn(28 * 28).nOut(500).build())
                .layer(new DenseLayer.Builder().nOut(100).build())
                .layer(new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                        .activation(Activation.SOFTMAX).nOut(10).build())
                .build();

        VoidConfiguration voidConfiguration = VoidConfiguration.builder()
                .unicastPort(40123)
                .networkMask("10.0.0.0/16")
                .controllerAddress(useSparkLocal ? "127.0.0.1" : null)
                .build();

        TrainingMaster tm = new SharedTrainingMaster.Builder(voidConfiguration, batchSizePerWorker)
                .updatesThreshold(1e-3)
                .rddTrainingApproach(RDDTrainingApproach.Direct)
                .batchSizePerWorker(batchSizePerWorker)
                .workersPerNode(4) // 4 workers for each Spark node
                .build();

        // Create the Spark network
        SparkDl4jMultiLayer sparkNet = new SparkDl4jMultiLayer(sc, conf, tm);

        // Execute training
        for (int i = 0; i < numEpochs; i++) {
            //sparkNet.fit(trainData);
            log.info("Completed Epoch {}", i);
        }
    }
}

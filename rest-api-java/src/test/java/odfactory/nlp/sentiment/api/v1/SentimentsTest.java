package odfactory.nlp.sentiment.api.v1;

import org.junit.Test;

import static org.junit.Assert.*;

public class SentimentsTest {

    @Test
    public void add() {
        Sentiment sentiment1 = new Sentiment();
        sentiment1.setLanguage("fr");
        Sentiment sentiment2 = new Sentiment();
        sentiment2.setLanguage("en");

        Sentiments sentiments = new Sentiments();
        assertEquals(0, sentiments.getItems().size());

        sentiments.add(sentiment1);
        sentiments.add(sentiment2);
        assertEquals(2, sentiments.getItems().size());
    }

    @Test
    public void get() {
        Sentiment sentiment1 = new Sentiment();
        sentiment1.setLanguage("fr");
        Sentiment sentiment2 = new Sentiment();
        sentiment2.setLanguage("en");
        Sentiments sentiments = new Sentiments();
        sentiments.add(sentiment1);
        sentiments.add(sentiment2);

        assertEquals("fr", sentiments.get(0).getLanguage());
    }

    @Test
    public void getItems() {
        Sentiment sentiment1 = new Sentiment();
        sentiment1.setLanguage("fr");
        Sentiment sentiment2 = new Sentiment();
        sentiment2.setLanguage("en");

        Sentiments sentiments = new Sentiments();
        assertEquals(0, sentiments.getItems().size());

        sentiments.add(sentiment1);
        sentiments.add(sentiment2);
        assertEquals(2, sentiments.getItems().size());
    }
}
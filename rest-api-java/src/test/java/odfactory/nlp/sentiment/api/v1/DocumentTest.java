package odfactory.nlp.sentiment.api.v1;

import org.junit.Test;

import static org.junit.Assert.*;

public class DocumentTest {

    @Test
    public void getLanguage() {
        Document document = new Document("fr", "my text");
        assertEquals("fr", document.getLanguage());
    }

    @Test
    public void getLanguageEmpty() {
        Document document = new Document();
        assertEquals("", document.getLanguage());
    }

    @Test
    public void setLanguage() {
        Document document = new Document("fr", "my text");
        assertEquals("fr", document.getLanguage());
        assertEquals("my text", document.getText());
        document.setLanguage("en");
        assertEquals("en", document.getLanguage());
        assertEquals("my text", document.getText());
    }

    @Test
    public void getText() {
        Document document = new Document("fr", "my text");
        assertEquals("my text", document.getText());
    }

    @Test
    public void getTextEmpty() {
        Document document = new Document();
        assertEquals("", document.getText());
    }

    @Test
    public void setText() {
        Document document = new Document("fr", "my text");
        assertEquals("fr", document.getLanguage());
        assertEquals("my text", document.getText());
        document.setText("new text");
        assertEquals("fr", document.getLanguage());
        assertEquals("new text", document.getText());
    }
}
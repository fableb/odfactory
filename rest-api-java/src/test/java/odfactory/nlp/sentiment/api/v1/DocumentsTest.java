package odfactory.nlp.sentiment.api.v1;

import org.junit.Test;

import static org.junit.Assert.*;

public class DocumentsTest {
    @Test
    public void add() {
        Document doc1 = new Document("fr", "text1");
        Document doc2 = new Document("en", "text2");
        Document doc3 = new Document("it", "text3");
        Documents documents = new Documents();

        assertEquals( 0, documents.getItems().size());
        documents.add(doc1);
        documents.add(doc2);
        documents.add(doc3);
        assertEquals( 3, documents.getItems().size());
    }

    @Test
    public void get() {
        Document doc1 = new Document("fr", "text1");
        Document doc2 = new Document("en", "text2");
        Document doc3 = new Document("it", "text3");
        Documents documents = new Documents();
        documents.add(doc1);
        documents.add(doc2);
        documents.add(doc3);

        Document item = documents.get(1);
        assertEquals( "en", item.getLanguage());
        assertEquals( "text2", item.getText());
    }

    @Test
    public void getItems() {
        Document doc1 = new Document("fr", "text1");
        Document doc2 = new Document("en", "text2");
        Document doc3 = new Document("it", "text3");
        Documents documents = new Documents();

        assertEquals( 0, documents.getItems().size());
        documents.add(doc1);
        documents.add(doc2);
        documents.add(doc3);
        assertEquals( 3, documents.getItems().size());
    }
}
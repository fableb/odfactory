package odfactory.nlp.sentiment.api.v1;

import org.junit.Test;

import static org.junit.Assert.*;

public class APITest {

    @Test
    public void getNameEmpty() {
        API api = new API();
        assertEquals("", api.getName());
    }

    @Test
    public void getName() {
        API api = new API("api name");
        assertEquals("api name", api.getName());
    }

    @Test
    public void setName() {
        API api = new API("api name1");
        assertEquals("api name1", api.getName());
        api.setName("api name2");
        assertEquals("api name2", api.getName());
    }

    @Test
    public void getVersion() {
        API api = new API();
        assertEquals("v1", api.getVersion());
    }
}
package odfactory.nlp.sentiment.api.v1;

/**
 * @author Fabrice LEBEL, fabrice.lebel.pro@outlook.com
 */
public class Document {
    private String language;
    private String text;

    public Document() {
        this.language = "";
        this.text = "";
    }

    public Document(String language, String text) {
        this.language = language;
        this.text = text;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

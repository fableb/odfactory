package odfactory.nlp.sentiment.api.v1;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * @author Fabrice LEBEL, fabrice.lebel.pro@outlook.com
 */
@Path("greetings")
public class Greetings {
    final private String message = "Welcome to Open Distributed NLP API v1!";

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        return message;
    }
}

package odfactory.nlp.sentiment.api.v1;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * @author Fabrice LEBEL, fabrice.lebel.pro@outlook.com
 */
public class Response {
    protected API api;

    public Response() {
        this.api = null;
    }

    public Response(API api) {
        this.api = api;
    }

    public API getApi() {
        return api;
    }

    public void setApi(API api) {
        this.api = api;
    }

    public String toJSON() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("name", this.api.getName());
        jsonObject.addProperty("version", this.api.getVersion());

        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public static void main(String[] args) {
        Response response = new Response(new API("Test"));
        System.out.println(response.toJSON());
    }
}

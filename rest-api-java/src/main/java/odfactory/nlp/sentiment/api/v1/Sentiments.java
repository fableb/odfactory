package odfactory.nlp.sentiment.api.v1;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Fabrice LEBEL, fabrice.lebel.pro@outlook.com
 */
public class Sentiments {
    private List<Sentiment> items;

    public Sentiments() {
        this.items = new ArrayList<>();
    }

    public void add(Sentiment sentiment) {
        this.items.add(sentiment);
    }

    public Sentiment get(int index) {
        return this.items.get(index);
    }

    public List<Sentiment> getItems() {
        return this.items;
    }
}

// *****************************************************************************
// *                                                                           *
// * SupportVectorMachineService:                                              *
// *                                                                           *
// * Support vector machine on TF-IDF matrix service.                          *
// *                                                                           *
// * Copyright (2019), Fabrice LEBEL (fabrice.lebel.pro@outlook.com)           *
// *                                                                           *
// *****************************************************************************
package odfactory.nlp.sentiment.api.v1;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.ml.PipelineModel;
import org.apache.spark.mllib.classification.SVMModel;
import org.apache.spark.mllib.linalg.SparseVector;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.util.MLUtils;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.List;

/**
 * @author Fabrice LEBEL, fabrice.lebel.pro@outlook.com
 */
@Path("v1/sentiment/svm")
public class SupportVectorMachineService {
    private final static String SERVICE_NAME = "SupportVectorMachineService";

    private Sentiments getSentimentAnalysis(Documents documents) {
        // Load configuration file
        // TODO

        // Load trained model
        SparkSession sparkSession = SparkSession.builder()
                .master("local")
                .appName(SERVICE_NAME)
                .getOrCreate().newSession();
        sparkSession.sparkContext().setLogLevel("ERROR");
        System.out.println("INFO: " + SERVICE_NAME + ": Connected to Apache Spark Server (version: "
                + sparkSession.sparkContext().version()
                + " sessionID: " + sparkSession.sessionState().toString() + ")");

        // Load TF-IDF matrix and trained model
        String modelsPath = "../trained-models/sparkml";
        System.out.println("INFO: " + SERVICE_NAME + ": Models path: " + modelsPath);

        PipelineModel tfidfPipeline = PipelineModel.load(modelsPath + "/Spark-TFIDF");
        System.out.println("INFO: " + SERVICE_NAME + ": TF-IDF matrix loaded");

        final SVMModel model = SVMModel.load(sparkSession.sparkContext(), modelsPath + "/Spark-SupportVectorMachineModel");
        System.out.println("INFO: " + SERVICE_NAME + ": Support Vector Machine model loaded");

        // Create a dataframe with the text message
        Dataset<Row> unlabeled = Tools.createDataframe(sparkSession, documents);

        // Create TF-IDF matrix from unlabeled data and transform the Dataframe to RDD
        Dataset<Row> unlabeledFitted = MLUtils.convertVectorColumnsFromML(
                tfidfPipeline.transform(unlabeled));
        /*
        for (int i = 0; i < unlabeledFitted.toJavaRDD().collect().toArray().length; i++) {
            System.out.println(unlabeledFitted.javaRDD().collect().toArray()[i]);
        }
         */

        JavaRDD<Vector> unlabeledFittedRDD = unlabeledFitted.select("tfidf").toJavaRDD()
                .map(new RowVectorFunction());
        /*
        for(int i=0; i < unlabeledFittedRDD.collect().toArray().length; i++){
            System.out.println(unlabeledFittedRDD.collect().toArray()[i]);
        }
         */

        // Predict Sentiment value
        List<Double> predictions = model.predict(unlabeledFittedRDD).collect();
        System.out.println("INFO: " + SERVICE_NAME + ": Sentiment values: " + predictions);

        // Get results
        Sentiments sentiments = new Sentiments();
        for (int i = 0; i < documents.getItems().size(); i++) {
            Document document = documents.get(i);
            double sentimentValue = predictions.get(i);

            Sentiment sentiment = new Sentiment();
            sentiment.setLanguage(document.getLanguage());
            sentiment.setText(document.getText());
            sentiment.setSentimentValue(sentimentValue);

            sentiments.add(sentiment);
        }

        // Close Spark session
        sparkSession.close();
        System.out.println("INFO: " + SERVICE_NAME + ": SessionID " + sparkSession.sessionState().toString() + " closed.");

        return sentiments;
    }

    // Example
    public static void main(String[] args) {
        Document doc1 = new Document("en", "this is a good movie");
        Document doc2 = new Document("en", "this is a bad movie");

        Documents documents = new Documents();
        documents.add(doc1);
        documents.add(doc2);

        SupportVectorMachineService service = new SupportVectorMachineService();
        Sentiments sentiments = service.getSentimentAnalysis(documents);

        API api = new API(API.SERVICE_SENTIMENT_ANALYSIS);
        SentimentResponse sentimentResponse = new SentimentResponse(api, sentiments);
        System.out.println(sentimentResponse.toJSON());
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postIt(String s) {
        Gson gson = new Gson();
        Request request = gson.fromJson(s, Request.class);
        System.out.println("INFO: " + SERVICE_NAME + ": POST request: " + request.toJSON());

        Sentiments sentiments = getSentimentAnalysis(request.getDocuments());

        API api = new API(API.SERVICE_SENTIMENT_ANALYSIS);
        SentimentResponse sentimentResponse = new SentimentResponse(api, sentiments);
        String json = sentimentResponse.toJSON();
        System.out.println("INFO: " + SERVICE_NAME + ": POST response: " + json);

        return Response.status(200).entity(json).build();
    }

    private static class RowVectorFunction implements Function<Row, Vector> {

        public SparseVector call(Row row) throws Exception {
            return (SparseVector) row.get(0);
        }
    }
}

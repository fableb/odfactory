package odfactory.nlp.sentiment.api.v1;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * @author Fabrice LEBEL, fabrice.lebel.pro@outlook.com
 */
public class Sentiment {
    public static final String METHOD_LOGISTIC_REGRESSION = "LogisticRegression";
    public static final String METHOD_RANDOM_FOREST = "RandomForest";
    public static final String METHOD_SUPPORT_VECTOR_MACHINE = "SupportVectorMachine";

    private String language;
    private String text;
    private double negativeProbability;
    private double positiveProbability;
    private double sentimentValue;

    public Sentiment() {
        this.language = "";
        this.text = "";
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public double getNegativeProbability() {
        return negativeProbability;
    }

    public void setNegativeProbability(double negativeProbability) {
        this.negativeProbability = negativeProbability;
    }

    public double getPositiveProbability() {
        return positiveProbability;
    }

    public void setPositiveProbability(double positiveProbability) {
        this.positiveProbability = positiveProbability;
    }

    public double getSentimentValue() {
        return sentimentValue;
    }

    public void setSentimentValue(double sentimentValue) {
        this.sentimentValue = sentimentValue;
    }

    public String toJSON() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("language", this.language);
        jsonObject.addProperty("text", this.text);
        jsonObject.addProperty("negativeProbability", this.negativeProbability);
        jsonObject.addProperty("positiveProbability", this.positiveProbability);
        jsonObject.addProperty("sentimentValue", this.sentimentValue);

        Gson gson = new Gson();
        return gson.toJson(this);
    }
}

// *****************************************************************************
// *                                                                           *
// * RandomForestService:                                                      *
// *                                                                           *
// * Random forests on TF-IDF matrix service.                                  *
// *                                                                           *
// * Copyright (2019), Fabrice LEBEL (fabrice.lebel.pro@outlook.com)           *
// *                                                                           *
// *****************************************************************************
package odfactory.nlp.sentiment.api.v1;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import org.apache.spark.ml.PipelineModel;
import org.apache.spark.ml.linalg.DenseVector;
import org.apache.spark.ml.classification.RandomForestClassificationModel;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.List;

/**
 * @author Fabrice LEBEL, fabrice.lebel.pro@outlook.com
 */
@Path("v1/sentiment/randomforest")
public class RandomForestService {
    private final static String SERVICE_NAME = "RandomForestService";

    private Sentiments getSentimentAnalysis(Documents documents) {
        // Load configuration file
        // TODO

        // Load trained model
        SparkSession sparkSession = SparkSession.builder()
                .master("local")
                .appName(SERVICE_NAME)
                .getOrCreate().newSession();
        sparkSession.sparkContext().setLogLevel("ERROR");
        System.out.println("INFO: " + SERVICE_NAME + ": Connected to Apache Spark Server (version: "
                + sparkSession.sparkContext().version()
                + " sessionID: " + sparkSession.sessionState().toString() + ")");

        // Load TF-IDF matrix and trained model
        String modelsPath = "../trained-models/sparkml";
        System.out.println("INFO: " + SERVICE_NAME + ": Models path: " + modelsPath);

        PipelineModel tfidfPipeline = PipelineModel.load(modelsPath + "/Spark-TFIDF");
        System.out.println("INFO: RandomForestService: TF-IDF matrix loaded");

        RandomForestClassificationModel model = RandomForestClassificationModel.load(modelsPath + "/Spark-RandomForestModel");
        System.out.println("INFO: " + SERVICE_NAME + ": Random forests model loaded");

        // Create a dataframe with the text message
        Dataset<Row> unlabeled = Tools.createDataframe(sparkSession, documents);

        // Create TF-IDF matrix from unlabeled data
        Dataset<Row> unlabeledFitted = tfidfPipeline.transform(unlabeled);

        // Predict Sentiment value
        List<Row> predictions = model
                .transform(unlabeledFitted)
                .select("id", "clean_text", "probability", "prediction")
                .collectAsList();

        // Get results
        Sentiments sentiments = new Sentiments();
        for (int i = 0; i < documents.getItems().size(); i++) {
            Document document = documents.get(i);
            Row prediction = predictions.get(i);
            System.out.println("INFO: " + SERVICE_NAME + ": Prediction: " + prediction);
            double[] probabilities = ((DenseVector) prediction.get(2)).toArray();
            double negProbability = probabilities[0];
            double posProbability = probabilities[1];
            double sentimentValue = prediction.getDouble(3);

            Sentiment sentiment = new Sentiment();
            sentiment.setLanguage(document.getLanguage());
            sentiment.setText(document.getText());
            sentiment.setNegativeProbability(negProbability);
            sentiment.setPositiveProbability(posProbability);
            sentiment.setSentimentValue(sentimentValue);

            sentiments.add(sentiment);
        }

        // Close Spark session
        sparkSession.close();
        System.out.println("INFO: " + SERVICE_NAME + ": SessionID " + sparkSession.sessionState().toString() + " closed.");

        return sentiments;
    }

    // Example
    public static void main(String[] args) {
        Document doc1 = new Document("en", "this is a good movie");
        Document doc2 = new Document("en", "this is a bad movie");

        Documents documents = new Documents();
        documents.add(doc1);
        documents.add(doc2);

        RandomForestService service = new RandomForestService();
        Sentiments sentiments = service.getSentimentAnalysis(documents);

        API api = new API(API.SERVICE_SENTIMENT_ANALYSIS);
        SentimentResponse sentimentResponse = new SentimentResponse(api, sentiments);
        System.out.println(sentimentResponse.toJSON());
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postIt(String s) {
        Gson gson = new Gson();
        Request request = gson.fromJson(s, Request.class);
        System.out.println("INFO: " + SERVICE_NAME + ": POST request: " + request.toJSON());

        Sentiments sentiments = getSentimentAnalysis(request.getDocuments());

        API api = new API(API.SERVICE_SENTIMENT_ANALYSIS);
        SentimentResponse sentimentResponse = new SentimentResponse(api, sentiments);
        String json = sentimentResponse.toJSON();
        System.out.println("INFO: " + SERVICE_NAME + ": POST response: " + json);

        return Response.status(200).entity(json).build();
    }
}

package odfactory.nlp.sentiment.api.v1;

/**
 * @author Fabrice LEBEL, fabrice.lebel.pro@outlook.com
 */
public class API {
    public static final String API_VERSION = "v1";
    public static final String SERVICE_SENTIMENT_ANALYSIS = "SentimentAnalysisService";
    public static final String SERVICE_POS_TAGGING = "POSTaggingService";

    private String name;
    private String version;

    public API() {
        this.name = "";
        this.version = API_VERSION;
    }

    public API(String name) {
        this.name = name;
        this.version = API_VERSION;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }
}

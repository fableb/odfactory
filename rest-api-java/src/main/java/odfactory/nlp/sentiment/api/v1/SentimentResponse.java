package odfactory.nlp.sentiment.api.v1;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * @author Fabrice LEBEL, fabrice.lebel.pro@outlook.com
 */
public class SentimentResponse extends Response {
    private Sentiments sentiments;

    public SentimentResponse() {
        this.sentiments = null;
    }

    public SentimentResponse(API api, Sentiments sentiments) {
        this.api = api;
        this.sentiments = sentiments;
    }

    public Sentiments getSentiments() {
        return sentiments;
    }

    public void setSentiments(Sentiments sentiments) {
        this.sentiments = sentiments;
    }

    public String toJSON() {
        JsonObject jsonObject = new JsonObject();
        this.api.setName(API.SERVICE_SENTIMENT_ANALYSIS);
        jsonObject.addProperty("name", this.api.getName());
        jsonObject.addProperty("version", this.api.getVersion());

        for(Sentiment sentiment: this.sentiments.getItems()) {
            jsonObject.addProperty("language", sentiment.getLanguage());
            jsonObject.addProperty("text", sentiment.getText());
            jsonObject.addProperty("negativeProbability", sentiment.getNegativeProbability());
            jsonObject.addProperty("positiveProbability", sentiment.getPositiveProbability());
            jsonObject.addProperty("sentimentValue", sentiment.getSentimentValue());
        }

        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public static void main(String[] args) {
        Sentiment sentiment1 = new Sentiment();
        sentiment1.setLanguage("en");
        sentiment1.setText("this is a good movie");
        Sentiment sentiment2 = new Sentiment();
        sentiment2.setLanguage("en");
        sentiment2.setText("this is a very bad movie");

        API api = new API(API.SERVICE_SENTIMENT_ANALYSIS);
        Sentiments sentiments = new Sentiments();
        sentiments.add(sentiment1);
        sentiments.add(sentiment2);

        SentimentResponse sentimentResponse = new SentimentResponse(api, sentiments);
        System.out.println(sentimentResponse.toJSON());
    }
}

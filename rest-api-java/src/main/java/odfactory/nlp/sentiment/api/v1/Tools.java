package odfactory.nlp.sentiment.api.v1;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Fabrice LEBEL, fabrice.lebel.pro@outlook.com
 */
public class Tools {
    public static String cleanText(String s) {
        return s.toLowerCase().replaceAll("[^a-zA-Z\\s]", "");
    }

    public static Dataset<Row> createDataframe(SparkSession sparkSession, Documents documents) {
        // Dataframe schema
        StructType schema = new StructType(new StructField[]{
                new StructField("id", DataTypes.LongType, false, Metadata.empty()),
                new StructField("text", DataTypes.StringType, false, Metadata.empty()),
                new StructField("clean_text", DataTypes.StringType, false, Metadata.empty())
        });

        // Create dataframe with provided document texts
        List<Row> rows = new ArrayList<>();
        long i = 0;
        for(Document doc: documents.getItems()) {
            rows.add(RowFactory.create(i, doc.getText(), cleanText(doc.getText())));
            i++;
        }
        String dummyText = "This is a dummy text to correct a bug in Spark. It needs at least 2 rows in the data frame.";
        String dummyTextCleaned = cleanText(dummyText);
        long end = documents.getItems().size() + 1;
        rows.add(RowFactory.create(end, dummyText, dummyTextCleaned));

        Dataset<Row> unlabeled = sparkSession.createDataFrame(rows, schema);
        return unlabeled;
    }
}

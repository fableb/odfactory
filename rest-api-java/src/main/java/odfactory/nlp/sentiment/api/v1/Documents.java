package odfactory.nlp.sentiment.api.v1;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Fabrice LEBEL, fabrice.lebel.pro@outlook.com
 */
public class Documents {
    private List<Document> items;

    public Documents() {
        this.items = new ArrayList<>();
    }

    void add(Document document) {
        this.items.add(document);
    }

    public Document get(int index) {
        return this.items.get(index);
    }

    public List<Document> getItems() {
        return items;
    }
}

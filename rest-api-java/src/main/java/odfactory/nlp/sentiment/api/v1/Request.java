package odfactory.nlp.sentiment.api.v1;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * @author Fabrice LEBEL, fabrice.lebel.pro@outlook.com
 */
public class Request {
    private Documents documents;

    public Request() {
        this.documents = null;
    }

    public Request(Documents documents) {
        this.documents = documents;
    }

    public Documents getDocuments() {
        return documents;
    }

    public void setDocuments(Documents documents) {
        this.documents = documents;
    }

    public String toJSON() {
        JsonObject jsonObject = new JsonObject();

        for (Document document : this.documents.getItems()) {
            jsonObject.addProperty("language", document.getLanguage());
            jsonObject.addProperty("text", document.getText());
        }

        Gson gson = new Gson();
        return gson.toJson(this);
    }

    // Example
    public static void main(String[] args) {
        Document doc1 = new Document("en", "this is a good movie");
        Document doc2 = new Document("en", "this is a bad movie");

        Documents documents = new Documents();
        documents.add(doc1);
        documents.add(doc2);

        Request request = new Request(documents);
        System.out.println(request.toJSON());
    }
}

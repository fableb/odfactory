"""
author: Fabrice LEBEL, fabrice.lebel.pro@outlook.com
"""

import os
from flask import Flask, request
from api.feedforward_service import FeedForwardService
from api.documents import Documents
from api.api import API
from api.sentiment_response import SentimentResponse

# Web server IP address and port
WEB_SERVER_IP = os.getenv("FLC_WEB_SERVER_IP", default="0.0.0.0")
WEB_SERVER_PORT = os.getenv("FLC_WEB_SERVER_PORT", default="8080")
KERAS_MODELS_BASE_PATH = "../../../trained-models/keras"

app = Flask(__name__)

# Load trained models
svc = FeedForwardService(KERAS_MODELS_BASE_PATH + "/tokenizer_tfidf_en.pickle",
                         KERAS_MODELS_BASE_PATH + "/ann_tfidf_en.h5")


@app.route('/api/v1/sentiment/feedforward', methods=['POST'])
def feed_forward():
    # Get JSON request
    rq = request.get_json()
    print('INFO: Request: ', rq)

    # Get documents
    docs = Documents.from_json(rq)
    print('INFO: Number of documents received: ', len(docs))
    print('INFO: Documents: ', docs)

    # Sentiment analysis
    sentiments = svc.get_sentiment_analysis(docs)
    print('INFO: Sentiments: ', sentiments)

    # Send JSON response
    api = API("FeedForwardService")
    rv = SentimentResponse(api, sentiments)
    print('INFO: Response: ', rv.dict())
    return str(rv)


if __name__ == '__main__':
    app.run(debug=True, host=WEB_SERVER_IP, port=WEB_SERVER_PORT)

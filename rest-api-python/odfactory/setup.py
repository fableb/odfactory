from setuptools import setup
from setuptools import find_packages

setup(name='odfactory',
      version='0.1.0',
      description='Open Distributed Factory',
      author='Fabrice LEBEL',
      author_email='fabrice.lebel.pro@outlook.com',
      packages=find_packages())

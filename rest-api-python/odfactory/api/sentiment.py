"""
author: Fabrice LEBEL, fabrice.lebel.pro@outlook.com
"""

from typing import Dict


class Sentiment:
    def __init__(self):
        self.language: str = ""
        self.text: str = ""
        self.negativeProbability: float = 0
        self.positiveProbability: float = 0
        self.sentimentValue: float = 0

    def __repr__(self):
        pass

    def __str__(self):
        pass

    def dict(self) -> Dict:
        return self.__dict__

"""
author: Fabrice LEBEL, fabrice.lebel.pro@outlook.com
"""

import json
from typing import Dict, List
from .document import Document


class Documents:
    def __init__(self):
        self.items: List[Document] = []

    def __repr__(self):
        return 'Documents()'

    def __len__(self):
        return len(self.items)

    def __str__(self):
        s = "["
        i = 0
        for item in self.items:
            s += "{}{}{}".format("{", item, "}")
            i += 1
            if i == len(self.items):
                s += "]"
            else:
                s += ","
        return s

    def add(self, doc: Document):
        self.items.append(doc)

    def get_texts(self) -> List[str]:
        """
        Returns text of all documents in a list of strings
        :return: list of strings
        """
        rv: List[str] = []
        for doc in self.items:
            rv.append(doc.text)
        return rv

    @staticmethod
    def from_json(dic: Dict):
        """
        Creates a Documents object from JSON dictionary
        :param dic: JSON dictionary
        :return: Documents object
        """
        rv = Documents()
        for item in dic['documents']['items']:
            rv.add(Document(item['language'], item['text']))
        return rv


class DocumentsEncoder(json.JSONEncoder):
    """
    Documents class JSON encoder for json.dumps(...)
    """

    def default(self, docs: Documents) -> Dict:
        if isinstance(docs, Documents):
            rv = []
            for item in docs.items:
                rv.append({"language": item.language, "text": item.text})
            return {"documents": {"items": rv}}
        else:
            raise ValueError('`docs` argument must be Documents class')

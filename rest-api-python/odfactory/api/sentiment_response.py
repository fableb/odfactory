"""
author: Fabrice LEBEL, fabrice.lebel.pro@outlook.com
"""

from typing import Dict
from .api import API
from .sentiments import Sentiments


class SentimentResponse:
    def __init__(self, api: API, sentiments: Sentiments):
        self.api: API = api
        self.sentiments: Sentiments = sentiments

    def __repr__(self):
        pass

    def __str__(self):
        return str(self.dict())

    def dict(self) -> Dict:
        return {'api': self.api.dict(), 'sentiments': self.sentiments.dict()}

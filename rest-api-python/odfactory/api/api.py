"""
author: Fabrice LEBEL, fabrice.lebel.pro@outlook.com
"""

from typing import Dict


class API:
    _API_VERSION: str = 'v1'

    def __init__(self, name: str = ""):
        self.name: str = name

    def __repr__(self):
        return 'API(self.name="")'

    def __str__(self):
        return 'name: {}, version: {}'.format(self.name, self._API_VERSION)

    def dict(self) -> Dict:
        return {'name': self.name, 'version': self._API_VERSION}

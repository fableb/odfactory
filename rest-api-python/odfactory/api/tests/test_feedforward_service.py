import unittest
from api.feedforward_service import FeedForwardService
from api.document import Document
from api.documents import Documents
from api.sentiment import Sentiment
from api.sentiments import Sentiments


class FeedForwardServiceTestCase(unittest.TestCase):
    def setUp(self):
        self.KERAS_MODELS_BASE_PATH = "trained-models"
        self.svc = FeedForwardService(self.KERAS_MODELS_BASE_PATH + "/tokenizer_tfidf_en.pickle",
                                      self.KERAS_MODELS_BASE_PATH + "/ann_tfidf_en.h5")

        self.doc1 = Document('en', 'this is a good comment')
        self.doc2 = Document('en', 'this is a bad comment')
        self.docs = Documents()
        self.docs.add(self.doc1)
        self.docs.add(self.doc2)

        self.sent1 = Sentiment()
        self.sent1.language = 'en'
        self.sent1.text = 'this is a good comment'
        self.sent1.negativeProbability = 0.4
        self.sent1.positiveProbability = 0.6
        self.sent1.sentimentValue = 1.0

        self.sent2 = Sentiment()
        self.sent2.language = 'en'
        self.sent2.text = 'this is a bad comment'
        self.sent2.negativeProbability = 0.7
        self.sent2.positiveProbability = 0.3
        self.sent2.sentimentValue = 0.0

        self.sents = Sentiments()
        self.sents.add(self.sent1)
        self.sents.add(self.sent2)

    def tearDown(self):
        pass

    def test_constructor(self):
        self.assertEqual(self.svc.tokenizer_path, self.KERAS_MODELS_BASE_PATH + "/tokenizer_tfidf_en.pickle")
        self.assertEqual(self.svc.model_path, self.KERAS_MODELS_BASE_PATH + "/ann_tfidf_en.h5")
        self.assertNotEqual(self.svc.tokenizer, None)
        self.assertNotEqual(self.svc.model, None)

    def test_get_sentiment_analysis(self):
        rv = self.svc.get_sentiment_analysis(self.docs)
        self.assertEqual(isinstance(rv, Sentiments), True)

        self.assertEqual(rv.items[0].language, self.sent1.language)
        self.assertEqual(rv.items[0].text, self.sent1.text)
        self.assertEqual(rv.items[0].sentimentValue, 1.0)

        self.assertEqual(rv.items[1].language, self.sent2.language)
        self.assertEqual(rv.items[1].text, self.sent2.text)
        self.assertEqual(rv.items[1].sentimentValue, 0.0)

import unittest
from api.api import API


class APITestCase(unittest.TestCase):
    def setUp(self):
        self.api = API()

    def tearDown(self):
        pass

    def test_empty_name(self):
        self.assertEqual("", self.api.name)

    def test_name(self):
        self.api.name = 'some name'
        self.assertEqual('some name', self.api.name)

    def test_version(self):
        self.assertEqual('v1', self.api._API_VERSION)

    def test_dict(self):
        tmp = API("SomeService")
        self.assertEqual({'name': 'SomeService', 'version': 'v1'}, tmp.dict())

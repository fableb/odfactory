import unittest
from api.sentiment import Sentiment


class SentimentTestCase(unittest.TestCase):
    def setUp(self):
        self.sent1 = Sentiment()
        self.sent1.language = 'en'
        self.sent1.text = 'this is some text'
        self.sent1.negativeProbability = 0.6
        self.sent1.positiveProbability = 0.4
        self.sent1.sentimentValue = 0.0

    def tearDown(self):
        pass

    def test_constructor(self):
        self.assertEqual('en', self.sent1.language)
        self.assertEqual('this is some text', self.sent1.text)
        self.assertEqual(0.6, self.sent1.negativeProbability)
        self.assertEqual(0.4, self.sent1.positiveProbability)
        self.assertEqual(0.0, self.sent1.sentimentValue)

    def test_dict(self):
        self.assertEqual(self.sent1.__dict__, self.sent1.dict())

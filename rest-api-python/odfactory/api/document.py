"""
author: Fabrice LEBEL, fabrice.lebel.pro@outlook.com
"""

import json
from typing import Dict


class Document:
    def __init__(self, language: str = "", text: str = ""):
        self.language = language
        self.text = text

    def __repr__(self):
        return 'Document(self.language="", self.text="")'

    def __str__(self):
        return 'language: {}, text: {}'.format(self.language, self.text)

    @staticmethod
    def json_decoder(dic):
        """
        JSON decoder for json.loads(..., object_hook=Document.json_decoder, ...)
        :param dic: Dictionary
        :return: Document object
        """
        rv = Document()
        if 'language' in dic:
            rv.language = dic['language']
        if 'text' in dic:
            rv.text = dic['text']
        return rv


class DocumentEncoder(json.JSONEncoder):
    """
    Document class JSON encoder for json.dumps(...)
    """

    def default(self, doc: Document) -> Dict:
        if isinstance(doc, Document):
            return {'language': doc.language, 'text': doc.text}
        else:
            raise ValueError('`doc` argument must be a Document class')

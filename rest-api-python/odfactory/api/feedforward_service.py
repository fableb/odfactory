"""
author: Fabrice LEBEL, fabrice.lebel.pro@outlook.com
"""

from .documents import Documents
from .sentiment import Sentiment
from .sentiments import Sentiments
import pickle  # To load saved Keras tokenizer
import keras
from keras.utils import CustomObjectScope
from keras.initializers import glorot_uniform


class FeedForwardService:
    _SERVICE_NAME: str = 'FeedForwardService'

    def __init__(self, tokenizer_path: str, model_path: str):
        """
        Feed Forward Neural Network on TF-IDF matrix for sentiment analysis

        :param tokenizer_path: Path to trained tokenizer model
        :param model_path: Path to trained model
        """
        self.tokenizer_path = tokenizer_path
        self.model_path = model_path

        keras.backend.clear_session()  # Clear TensorFlow backend

        self.tokenizer = None
        with open(tokenizer_path, 'rb') as handle:
            self.tokenizer = pickle.load(handle)

        self.model = None
        with CustomObjectScope({'GlorotUniform': glorot_uniform()}):
            self.model = keras.models.load_model(self.model_path)
        self.model._make_predict_function()

    def __repr__(self):
        pass

    def __str__(self):
        pass

    def model_summary(self):
        return self.model.summary()

    def get_sentiment_analysis(self, documents: Documents) -> Sentiments:
        """
        Compute sentiment classes for a list of documents

        :param documents: a Documents class instance
        :return: a Sentiments class instance
        """
        tfidf_matrix = self.tokenizer.texts_to_matrix(documents.get_texts(), mode='tfidf')
        predictions = self.model.predict_classes(tfidf_matrix, verbose=1)
        rv = Sentiments()
        for i in range(len(documents)):
            sentiment = Sentiment()
            sentiment.language = documents.items[i].language
            sentiment.text = documents.items[i].text
            sentiment.negativeProbability = 0.0
            sentiment.positiveProbability = 0.0
            sentiment.sentimentValue = predictions[i]
            rv.add(sentiment)
        return rv

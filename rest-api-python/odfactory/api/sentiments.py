"""
author: Fabrice LEBEL, fabrice.lebel.pro@outlook.com
"""

from typing import Dict, List
from .sentiment import Sentiment


class Sentiments:
    def __init__(self):
        self.items: List[Sentiment] = []

    def __repr__(self):
        pass

    def __str__(self):
        return str(self.dict())

    def add(self, sentiment: Sentiment):
        self.items.append(sentiment)

    def dict(self) -> Dict:
        return {'items': [x.__dict__ for x in self.items]}

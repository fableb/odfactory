"""
author: Fabrice LEBEL, fabrice.lebel.pro@outlook.com
"""

from typing import Dict
from .documents import Documents


class Request:
    def __init__(self, documents: Documents):
        self.documents: Documents = documents

    def __repr__(self):
        return 'Request(documents)'

    def __str__(self):
        pass

    def dict(self) -> Dict:
        pass
